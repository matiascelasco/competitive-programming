import Control.Monad
import qualified Data.Vector

initial_height = 1
upper_bound = 60
cycles_of_growth = cycle [(* 2), (+ 1)]
all_solutions = scanl (flip ($)) initial_height cycles_of_growth
solutions_vector = Data.Vector.fromList $ take (upper_bound + 1) $ all_solutions
solve_test n = solutions_vector Data.Vector.! n

main :: IO ()
main = do
  t <- readLn :: IO Int
  lines <- replicateM t getLine
  let 
    tests = map read lines
    ans = map solve_test tests
  mapM_ print ans