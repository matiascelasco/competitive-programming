--module CutTheSticks where
import Data.List (foldl')

line_values :: String -> [Int]
line_values = (map read) . words

apply_cut :: [Int] -> [Int]
apply_cut sticks_lengths = map (+ (- smallest)) $ filter (> smallest) sticks_lengths
    where smallest = minimum sticks_lengths

solve :: [Int] -> [Int]
solve = map length . takeWhile ((> 0) . length) . iterate apply_cut

main :: IO ()
main = do
    n <- readLn :: IO Int
    line <- getLine :: IO String
    let
        sticks_lengths = line_values line
        ans = solve sticks_lengths
    mapM_ print ans