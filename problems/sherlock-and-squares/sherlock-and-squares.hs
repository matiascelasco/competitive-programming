import Control.Monad (replicateM, mapM_)

solveTestCase :: (Int, Int) -> Int
solveTestCase (a, b) = length $ takeWhile (<= b) $ dropWhile (< a) squares
    where
    squares = map (^(2::Int)) [1..]

main :: IO ()
main = do
    t <- readLn
    let
        asPair [a, b] = (a, b)
        asPair _ = error "The list should have exactly 2 elements"
        readTestcase = getLine >>= (\str -> return $ asPair $ map read $ words str)
    testCases <- replicateM t readTestcase
    let
        ans = map solveTestCase testCases
    mapM_ print ans