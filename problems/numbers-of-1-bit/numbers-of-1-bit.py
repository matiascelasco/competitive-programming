def reversed_bits(n):
    if n == 0:
        yield 0
    else:
        while n > 0:
            yield n % 2
            n /= 2

class Solution:
    # @param n, an integer
    # @return an integer
    def hammingWeight(self, n):
        count = 0
        for bit in reversed_bits(n):
            if bit == 1:
                count += 1
        return count
