https://leetcode.com/discuss/27609/short-code-by-time-the-count-and-another-several-method-time

int hammingWeight(uint32_t n){
    int res = 0;
    while(n){
        n &= n - 1;
        ++ res;
    }
    return res;
}