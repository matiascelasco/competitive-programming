class Solution:
    # @param {string} s
    # @return {integer}
    def calculate(self, s):

        def evalValue(s):
            for i in range(len(s)-1,-1,-1):
                if s[i]=='*':
                    return evalValue(s[:i])*int(s[i+1:])
                if s[i]=='/':
                    return evalValue(s[:i])/int(s[i+1:])
            return int(s)
        res=0
        exps=s.split('+')
        exps=map(lambda x:x.split('-'),exps)
        for a in exps:
            tmp=evalValue(a[0])
            for i in range(1,len(a)):
                tmp-=evalValue(a[i])
            res+=tmp
        return res