def tokenizer(s):
    cur_int = []
    for c in s:
        if c == ' ':
            continue
        if c.isdigit():
            cur_int.append(c)
        else:
            if cur_int:
                yield int(''.join(cur_int))
                cur_int = []
            yield c
    if cur_int:
        yield int(''.join(cur_int))


def operate(o1, op, o2):
    if op == '+':
        return o1 + o2
    if op == '-':
        return o1 - o2
    if op == '*':
        return o1 * o2
    if op == '/':
        if (o1 < 0) != (o2 < 0):
            return - (o1 / (-o2))  # because Python has floor division
        else:
            return o1 / o2


def there_is_an_operator_on_top(stack):
    return stack and stack[-1] in '+-*/'


def there_is_a_high_precedende_operator_on_top(stack):
    return stack and stack[-1] in '*/'


def convert_to_inverse_polish(expr):
    op_stack = []
    for t in tokenizer(expr):
        if type(t) is int:
            yield t
        elif t in '+-':
            while there_is_an_operator_on_top(op_stack):
                yield op_stack.pop()
            op_stack.append(t)
        elif t in '*/':
            while there_is_a_high_precedende_operator_on_top(op_stack):
                yield op_stack.pop()
            op_stack.append(t)
        elif t == '(':
            op_stack.append(t)
        elif t == ')':
            while there_is_an_operator_on_top(op_stack):
                yield op_stack.pop()
            # assert op_stack[-1] == '('
            op_stack.pop()
    while there_is_an_operator_on_top(op_stack):
        yield op_stack.pop()


def calc_inverse_polish(tokens):
    stack = []
    for t in tokens:
        if type(t) is int:
            stack.append(t)
        else:
            # assert t in '+-*/'
            o2 = stack.pop()
            o1 = stack.pop()
            stack.append(operate(o1, t, o2))
    # assert len(stack) == 1
    return stack.pop()


def calculate(expr):
    return calc_inverse_polish(convert_to_inverse_polish(expr))

print calculate(raw_input())
