#include <iostream>
using namespace std;

typedef unsigned int uint;

int main(){

    uint t, x;
    cin >> t;
    while (t--){
        cin >> x;
        cout << ~x << endl;
    }

    return 0;
}