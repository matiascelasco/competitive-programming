from collections import defaultdict


def slope(a, b):
    if (a.x, a.y) == (b.x, b.y):
        return '='  # same point
    if a.x == b.x:
        return 'v'  # vertical
    if a.y == b.y:
        return 'h'  # horizontal
    x, y = a.x - b.x, a.y - b.y
    return norm(y, x)


def gcd(num, den):
    if den == 0:
        return num
    return gcd(den, num % den)


def norm(num, den):
    g = gcd(num, den)
    return num / g, den / g


def intercept(a, b, rect_slope=None):
    if rect_slope == '=':
        return None
    if rect_slope == 'v':
        return a.x
    if rect_slope == 'h':
        return a.y
    x, y = a.x, a.y
    return norm(y * rect_slope[1] - rect_slope[0] * x, rect_slope[1])


class Solution:
    # @param {Point[]} points
    # @return {integer}
    def maxPoints(self, points):
        if len(points) < 3:
            return len(points)

        rect_counter = defaultdict(lambda: set())
        for i in range(len(points)):
            for j in range(i + 1, len(points)):
                pi, pj = points[i], points[j]
                m = slope(pi, pj)
                if m != '=':
                    b = intercept(pi, pj, m)
                    rect_counter[m, b].add(i)
                    rect_counter[m, b].add(j)
                else:
                    rect_counter['=', (pi.x, pi.y)].add(i)
                    rect_counter['=', (pi.x, pi.y)].add(j)
        return reduce(max, map(len, rect_counter.itervalues()))

# --------------------------------------------------------------------------------------------------


class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b

n = input()
point_from_list = lambda xs: Point(xs[0], xs[1])
points = [point_from_list(map(int, raw_input().split(' '))) for _ in xrange(n)]
print Solution().maxPoints(points)
