isDivisibleBy :: Int -> Int -> Bool
isDivisibleBy a b = a `mod` b == 0

digitsSum :: Int -> Int
digitsSum n | n < 10 = n
            | otherwise = n `mod` 10 + digitsSum (n `div` 10)

factorization :: Int -> [Int]
factorization = facs 2
    where
    facs _ 1 = []
    facs i n | n `isDivisibleBy` i = i:(facs i (n `div` i))
             | otherwise = facs (i + 1) n

isSmith :: Int -> Bool
isSmith n = (not isPrime) && digitsSum n == sum (map digitsSum factors)
    where
    isPrime = length factors == 1
    factors = factorization n

solve :: Int -> Int
solve n = if isSmith n then 1 else 0


main :: IO ()
main = do
    n <- readLn
    let
        ans = solve n
    print ans