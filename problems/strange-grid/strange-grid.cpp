#include <iostream>
using namespace std;

typedef unsigned long long int ulint;

ulint f(ulint r, ulint c){
    if (r % 2 == 0) {
        return f(r - 1, c) + 1;
    }
    if (c > 1){
        return f(r, c - 1) + 2;
    }
    return (r - 1) * 5;
}

int main(){
    ulint r, c;
    cin >> r >> c;
    cout << f(r, c) << endl;
}