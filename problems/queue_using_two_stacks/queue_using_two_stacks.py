def move_stacks(source, destination):
    while len(source) > 0:
        destination.append(source.pop())


class Queue:
    # initialize your data structure here.
    def __init__(self):
        self.pop_stack = []
        self.push_stack = []

    # @param x, an integer
    # @return nothing
    def push(self, x):
        self.__prepare_for_push()
        self.push_stack.append(x)

    # @return nothing
    def pop(self):
        self.__prepare_for_pop()
        self.pop_stack.pop()


    # @return an integer
    def peek(self):
        self.__prepare_for_pop()
        return self.pop_stack[-1]


    # @return an boolean
    def empty(self):
        return len(self.push_stack) + len(self.pop_stack) == 0

    def __prepare_for_pop(self):
        if len(self.pop_stack) == 0:
            move_stacks(self.push_stack, self.pop_stack)

    def __prepare_for_push(self):
        if len(self.push_stack) == 0:
            move_stacks(self.pop_stack, self.push_stack)