#include <iostream>
using namespace std;

const int MAX_SIZE = 100000;

typedef unsigned long long int num;
typedef unsigned int uint;

uint ts, n;
num a[MAX_SIZE];
num pre[MAX_SIZE];
num post[MAX_SIZE];

int main(){

    cin >> ts;

    while (ts--){
        cin >> n;
        for (uint i = 0; i < n; i++){
            cin >> a[i];
        }
        pre[0] = 0;
        for (uint i = 1; i < n; i++){
            pre[i] = pre[i - 1] + a[i - 1];
        }
        post[n - 1] = 0;
        for (uint i = n - 1; i > 0; i--){
            post[i - 1] = post[i] + a[i];
        }

        bool flag = false;
        for (uint i = 0; i < n; i++){
            if (pre[i] == post[i]){
                flag = true;
                break;
            }
        }
        cout << (flag ? "YES" : "NO") << endl;

    }

    return 0;
}