class Solution:
    # @param s, a string
    # @param wordDict, a set<string>
    # @return a boolean
    def wordBreak(self, s, wordDict):
        mem = [None] * len(s)

        def find(s, pos, w):
            if len(s) - pos < len(w):
                return False
            for c in w:
                if s[pos] != c:
                    return False
                pos += 1
            return True

        def match(start):
            return (w for w in wordDict if find(s, start, w))

        def word_break(start):
            if start == len(s):
                return True
            if mem[start] is not None:
                return mem[start]
            for w in match(start):
                if word_break(start + len(w)):
                    mem[start] = True
                    return mem[start]
            mem[start] = False
            return mem[start]

        return word_break(0)
