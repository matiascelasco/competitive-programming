#include <iostream>
#include <set>
using namespace std;

int s[] = {2, 3, 5, 7, 11, 13};

int grundy[9];

int main(){

    grundy[0] = 0;
    for (int n = 1; n <= 8; n++){
        set<int> st;
        for (int i = 0; i < 6; i++){
            if (n - s[i] >= 0){
                st.insert(grundy[n - s[i]]);
            }
        }
        grundy[n] = 0;
        while (st.count(grundy[n])){
            grundy[n]++;
        }
    }

    int t, n;
    unsigned long long int x;
    cin >> t;
    while (t--){
        cin >> n;
        int total_xor = 0;
        while (n--){
            cin >> x;
            total_xor = total_xor xor grundy[x % 9];
        }
        cout << (total_xor ? "Manasa" : "Sandy") << endl;
    }

    return 0;
}