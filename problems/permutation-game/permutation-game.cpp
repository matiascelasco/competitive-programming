#include <iostream>
#include <cmath>
using namespace std;

bool result_is_known[32768];
bool result[32768];
int a[15];
int t, n;
int two_pow_n;

bool WINNER = true;
bool LOSER = false;

bool is_terminal(int position){
    int prev = 0;
    for (int i = 0, mask = two_pow_n / 2; i < n; i++, mask /= 2){
        if ((position bitand mask) > 0){
            int current = a[i];
            if (!(prev < current)){
                return false;
            }
            prev = current;
        }
    }
    return true;
}

bool remember(int position, bool value){
    result_is_known[position] = true;
    return result[position] = value;
}

bool get_result(int position){

    if (result_is_known[position]){
        return result[position];
    }

    if (is_terminal(position)){
        return remember(position, LOSER);
    }

    for (int mask = two_pow_n / 2; mask > 0; mask /= 2){
        if ((position bitand mask) > 0){
            int next_position = position xor mask;
            if (get_result(next_position) == LOSER){
                return remember(position, WINNER);
            }
        }
    }

    return remember(position, LOSER);
}

bool get_result(){
    return get_result(pow(2, n) - 1);
}

int main(){

    cin >> t;

    while (t--){
        cin >> n;
        two_pow_n = pow(2, n);
        for (int i = 0; i < n; i++){
            cin >> a[i];
        }
        for (int i = 0; i < two_pow_n; i++){
            result_is_known[i] = false;
        }

        cout << (get_result() ? "Alice" : "Bob") << endl;

    }

    return 0;
}