#include <iostream>
using namespace std;

int main(){
    int t;
    unsigned long long int n;
    cin >> t;
    while (t--){
        cin >> n;
        int zeroes_before_first_one = 0;
        int ones = 0;
        while (n > 1 and n % 2 == 0){
            n /= 2;
            zeroes_before_first_one++;
        }
        while (n > 1){
            ones += n % 2;
            n /= 2;
        }
        int total_movements = zeroes_before_first_one + ones;
        cout << ((total_movements % 2 == 0) ? "Richard" : "Louise") << endl;
    }
}