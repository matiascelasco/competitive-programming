# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
    # @param {ListNode} head
    # @return {void} Do not return anything, modify head in-place instead.
    def reorderList(self, head):
        if head is None:
            return None
        s = []
        cur = head
        while cur is not None:
            s.append(cur)
            cur = cur.next
        tail = s.pop()
        while head is not tail and head.next is not tail:
            s[-1].next = None
            tail.next = head.next
            head.next = tail
            head = tail.next
            tail = s.pop()
