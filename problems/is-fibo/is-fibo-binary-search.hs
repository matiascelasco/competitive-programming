import Control.Monad (replicateM, mapM_)
import Data.Vector (fromList, (!), Vector)
import qualified Data.Vector (length)


fibos :: [Integer]
fibos = fiboRec 1 1
    where
    fiboRec prev cur = cur:(fiboRec cur (prev + cur))

limit :: Integral t => t
limit = 10 ^ 10

fibosVector :: Vector Integer
fibosVector = fromList $ takeWhile (<= limit) fibos

binarySearch :: Vector Integer -> Integer -> Bool
binarySearch vector target = bs vector 0 (Data.Vector.length vector - 1) target
    where
    bs v i j x | i > j = False
               | (v ! mid) < x = bs v (mid + 1) j  x
               | (v ! mid) > x = bs v i (mid - 1)  x
               | otherwise = True
               where
               mid = (i + j) `div` 2

isFibo :: Integer -> Bool
isFibo n = binarySearch fibosVector n

solveTest :: Integer -> String
solveTest n = if isFibo n then "IsFibo" else "IsNotFibo"


main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readLn
    let
        ans = map solveTest testCases
    mapM_ putStrLn ans