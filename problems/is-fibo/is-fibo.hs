import Control.Monad (replicateM, mapM_)


fibos :: [Integer]
fibos = fiboRec 1 1
    where
    fiboRec prev cur = cur:(fiboRec cur (prev + cur))

someFibos :: [Integer]
someFibos = takeWhile (<= 10 ^ 10) fibos

isFibo :: Integer -> Bool
isFibo n = elem n someFibos

solveTest :: Integer -> String
solveTest n = if isFibo n then "IsFibo" else "IsNotFibo"


main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readLn
    let
        ans = map solveTest testCases
    mapM_ putStrLn ans