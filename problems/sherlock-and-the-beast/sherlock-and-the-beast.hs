import Control.Monad (replicateM, mapM_)
import Data.List (find)

isDivBy :: Int -> Int -> Bool
isDivBy a b = a `mod` b == 0

solveTestCase :: Int -> String
solveTestCase n =
    case find canUseIt [n, (n - 1)..0] of
        (Nothing) -> "-1"
        (Just numFives) -> buildDecent numFives
    where
        canUseIt numFives = numFives `isDivBy` 3 && (n - numFives) `isDivBy` 5
        buildDecent numFives = replicate numFives '5' ++ replicate (n - numFives) '3'

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readLn
    let
        ans = map solveTestCase testCases
    mapM_ putStrLn ans