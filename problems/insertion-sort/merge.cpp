#include <iostream>
using namespace std;

int array[100000];

long long int merge_and_count(int a[], int size){
    if (size < 2){
        return 0;
    }
    int left_size = size / 2;
    int right_size = size - left_size;
    int *left = a;
    int *right = a + left_size;

    long long int ans = merge_and_count(left, left_size) +
                        merge_and_count(right, right_size);

    int l = 0;
    int r = 0;
    long temp[size];
    while (l < left_size and r < right_size){
        if (left[l] <= right[r]){
            temp[l + r] = left[l];
            l++;
        }
        else {
            ans += left_size - l;
            temp[l + r] = right[r];
            r++;
        }
    }
    while (l < left_size){
        temp[l + r] = left[l];
        l++;
    }
    while (r < right_size){
        temp[l + r] = right[r];
        r++;
    }

    for (int i = 0; i < size; i++){
        a[i] = temp[i];
    }
    return ans;
}

int main(){

    int ts;
    cin >> ts;
    while (ts--){
        int n;
        cin >> n;
        for (int i = 0; i < n; i++){
            cin >> array[i];
        }
        cout << merge_and_count(array, n) << endl;
    }

    return 0;
}