#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int ts, n;

    cin >> ts;
    while (ts--){
        int x;
        long long ans = 0;
        vector<int> v;
        cin >> n;
        for (int i = 0; i < n; i++){
            cin >> x;
            vector<int>::iterator it = upper_bound(v.begin(), v.end(), x);
            ans += v.end() - it;
            v.insert(it, x);
        }
        cout << ans << endl;
    }
}