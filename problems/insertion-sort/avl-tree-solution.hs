import Control.Monad (replicateM)

data AVL t = EmptyAVL | AVL { value :: t
                            , count :: Int
                            , _size :: Int
                            , _height :: Int
                            , left :: AVL t
                            , right :: AVL t
                            }

height :: (AVL t) -> Int
height EmptyAVL = 0
height t = _height t

size :: (AVL t) -> Int
size EmptyAVL = 0
size t = _size t

balanceFactor :: (AVL t) -> Int
balanceFactor EmptyAVL = 0
balanceFactor t | _height t == 1 = 0
                | otherwise = (height $ left $ t) - (height $ right $ t)

rotateLeft :: (AVL t) -> (AVL t)
rotateLeft t = (left t) {
        _size = size t,
        _height = 1 + max (height l) (height r),
        left = l,
        right = r
    }
    where
    l = t1
    r = t {
        _size = sum [size t2, size t3, count t],
        _height = 1 + max (height t2) (height t3),
        left = t2, right = t3
    }
    t1 = left (left t)
    t2 = right (left t)
    t3 = right t

rotateRight :: (AVL t) -> (AVL t)
rotateRight t = (right t) {
        _size = size t,
        _height = 1 + max (height l) (height r),
        left = l, right = r
    }
    where
    l = t {
        _size = sum [size t2, size t3, count t],
        _height = 1 + max (height t2) (height t3),
        left = t3, right = t2
    }
    r = t1
    t1 = right (right t)
    t2 = left (right t)
    t3 = left t

rotateDoubleLeft :: (AVL t) -> (AVL t)
rotateDoubleLeft t = rotateLeft $ t {left = rotateRight (left t)}

rotateDoubleRight :: (AVL t) -> (AVL t)
rotateDoubleRight t = rotateRight $ t {right = rotateLeft (right t)}

insert :: (Ord t) => (AVL t) -> t -> (AVL t)
insert EmptyAVL val = AVL {value=val, count=1, _size=1, _height=1, left=EmptyAVL, right=EmptyAVL}
insert t x
    | isLL = rotateLeft updatedNode
    | isRR = rotateRight updatedNode
    | isRL = rotateDoubleRight updatedNode
    | isLR = rotateDoubleLeft updatedNode
    | otherwise = updatedNode
    where
        isLL = f > 1 && lf > 0
        isLR = f > 1 && lf < 0
        isRL = f < (-1) && rf > 0
        isRR = f < (-1) && rf < 0
        f = balanceFactor updatedNode
        lf = balanceFactor $ left updatedNode
        rf = balanceFactor $ right updatedNode
        updatedNode
            | x > value t = t {
                _size = size t + 1,
                _height =  1 + max (height (left t)) (height insertedAtRight),
                right = insertedAtRight
            }
            | x < value t = t {
                _size = size t + 1,
                _height = 1 + max (height insertedAtLeft) (height (right t)),
                left = insertedAtLeft
            }
            | otherwise = t {
                count = count t + 1,
                _size = size t + 1
            }
           where
           insertedAtLeft = insert (left t) x
           insertedAtRight = insert (right t) x

countGt :: (AVL Int) -> Int -> Int
countGt EmptyAVL _ = 0
countGt t x | x < value t = count t + size (right t) + countGt (left t) x
            | x > value t = countGt (right t) x
            | otherwise = size (right t)

solveTestCase :: [Int] -> Int
solveTestCase nums = solve 0 EmptyAVL nums
    where
    solve :: Int -> (AVL Int) -> [Int] -> Int
    solve accum _ [] = accum
    solve accum t (x:xs) = solve (accum + countGt t x) (insert t x) xs


readTestCase :: IO [Int]
readTestCase = do
    _ <- getLine
    line <- getLine
    return (map read $ words line)

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTestCase testCases
    mapM_ print ans