import Control.Monad (replicateM)
import Data.List (sort)

data Tree23 t =
    EmptyTree23 |
    TreeNode2 {
        value :: t,
        count :: Int,
        _size :: Int,
        left :: (Tree23 t),
        right :: (Tree23 t)
    } |
    TreeNode3 {
        valueLeft :: t,
        valueRight :: t,
        countLeft :: Int,
        countRight :: Int,
        _size :: Int,
        left :: (Tree23 t),
        center :: (Tree23 t),
        right :: (Tree23 t)
    }



isLeaf :: (Tree23 t) -> Bool
isLeaf EmptyTree23 = False
isLeaf TreeNode2 {left = EmptyTree23, right = EmptyTree23} = True
isLeaf TreeNode3 {left = EmptyTree23, right = EmptyTree23} = True
isLeaf _ = False

insert :: (Ord t) => (Tree23 t) -> t -> (Tree23 t)
insert EmptyTree23 newValue = TreeNode2 {
    value = newValue,
    count = 1,
    left = EmptyTree23,
    right = EmptyTree23,
    _size = 1
}
insert t@(TreeNode2 {}) newValue
    | newValue < value t =
        let
        (newLeft, splitted) = insertAndMaybeSplit (left t) newValue in
        if isLeaf t then TreeNode3 {
            valueLeft = newValue,
            valueRight = value t,
            countLeft = 1,
            countRight = count t,
            left = EmptyTree23,
            center = EmptyTree23,
            right = EmptyTree23,
            _size = size t + 1
        }
        else if not splitted then t {
            left = newLeft,
            _size = size t + 1
        }
        else TreeNode3 {
            left = left newLeft,
            valueLeft = value newLeft,
            countLeft = count newLeft,
            center = right newLeft,
            valueRight = value t,
            countRight = count t,
            right = right t,
            _size = size t + 1
        }
    | newValue > value t =
        let
        (newRight, splitted) = insertAndMaybeSplit (right t) newValue in
        if isLeaf t then TreeNode3 {
            valueLeft = value t,
            valueRight = newValue,
            countLeft = count t,
            countRight = 1,
            left = EmptyTree23,
            center = EmptyTree23,
            right = EmptyTree23,
            _size = size t + 1
        }
        else if not splitted then t {
            right = newRight,
            _size = size t + 1
        }
        else TreeNode3 {
            left = left t,
            valueLeft = value t,
            countLeft = count t,
            center = left newRight,
            valueRight = value newRight,
            countRight = count newRight,
            right = right newRight,
            _size = size t + 1
        }
    | otherwise = t {  -- newValue == value t
        count = count t + 1,
        _size = size t + 1
    }
insert t@(TreeNode3 {}) newValue = fst $ insertAndMaybeSplit t newValue

createNode :: t -> Int -> (Tree23 t)
createNode newValue newCount = TreeNode2 {
        value = newValue,
        count = newCount,
        left = EmptyTree23,
        right = EmptyTree23,
        _size = newCount
    }

insertAndMaybeSplit :: (Ord t) => (Tree23 t) -> t -> (Tree23 t, Bool)
insertAndMaybeSplit EmptyTree23 _ = error "It doesn't make sense to do this on an empty tree"
insertAndMaybeSplit t@(TreeNode2 {}) newValue = (insert t newValue, False)
insertAndMaybeSplit t@(TreeNode3 {}) newValue
    | newValue == valueLeft t = (t {countLeft = countLeft t + 1, _size = size t + 1}, False)
    | newValue == valueRight t = (t {countRight = countRight t + 1, _size = size t + 1}, False)
    | isLeaf t =
        let
        values = [(newValue, 1), (valueLeft t, countLeft t), (valueRight t, countRight t)]
        [(vl, cl), (cv, cc), (vr, cr)] = sort values in
        (TreeNode2 {
            value = cv,
            count = cc,
            left = createNode vl cl,
            right = createNode vr cr,
            _size = cl + cc + cr
        }, True)
    | newValue < valueLeft t =
        let (newLeft, splitted) = insertAndMaybeSplit (left t) newValue in
        if not splitted then (t {
            left = newLeft,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = valueLeft t,
            count = countLeft t,
            left = newLeft,
            right = TreeNode2 {
                value = valueRight t,
                count = countRight t,
                left = center t,
                right = right t,
                _size = size (right t) + size (center t) + countRight t
            },
            _size = size t + 1
        }, True)
    | newValue > valueRight t =
        let (newRigth, splitted) = insertAndMaybeSplit (right t) newValue in
        if not splitted then (t {
            right = newRigth,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = valueRight t,
            count = countRight t,
            left = TreeNode2 {
                value = valueLeft t,
                count = countLeft t,
                left = left t,
                right = center t,
                _size = size (left t) + size (center t) + countLeft t
            },
            right = newRigth,
            _size = size t + 1
        }, True)
    | otherwise = -- center
        let (newCenter, splitted) = insertAndMaybeSplit (center t) newValue in
        if not splitted then (t {
            center = newCenter,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = value newCenter,
            count = count newCenter,
            left = TreeNode2 {
                value = valueLeft t,
                count = countLeft t,
                left = left t,
                right = left newCenter,
                _size = sum (map (size . left) [t, newCenter]) + countLeft t
            },
            right = TreeNode2 {
                value = valueRight t,
                count = countRight t,
                left = right newCenter,
                right = right t,
                _size = sum (map (size . right) [t, newCenter]) + countRight t
            },
            _size = size t + 1
        }, True)


size :: (Tree23 t) -> Int
size EmptyTree23 = 0
size t = _size t

countEq :: (Ord t) => (Tree23 t) -> t -> Int
countEq EmptyTree23 _ = 0
countEq t@(TreeNode2{}) target
    | target < value t = countEq (left t) target
    | value t < target = countEq (right t) target
    | otherwise = count t
countEq t@(TreeNode3{}) target
    | target < valueLeft t = countEq (left t) target
    | target == valueLeft t = countLeft t
    | target < valueRight t = countEq (center t) target
    | target == valueRight t = countRight t
    | otherwise = countEq (right t) target

countLt :: (Ord t) => (Tree23 t) -> t -> Int
countLt EmptyTree23 _ = 0
countLt t@(TreeNode2{left = l, right = r}) target
    | target < value t = countLt l target
    | target == value t = size l
    | otherwise = size l + count t + countLt r target
countLt t@(TreeNode3{left = l, center = c, right = r}) target
    | target < valueLeft t = countLt l target
    | target == valueLeft t = size l
    | target < valueRight t = size l + countLeft t + countLt c target
    | target == valueRight t = size l + countLeft t + size c
    | otherwise = size l + countLeft t + size c + countRight t + countLt r target

countGt :: (Ord t) => (Tree23 t) -> t -> Int
countGt t target = size t - countLte t target

countLte :: (Ord t) => (Tree23 t) -> t -> Int
countLte t target = countLt t target + countEq t target

solveTestCase :: [Int] -> Int
solveTestCase nums = solve 0 EmptyTree23 nums
    where
    solve :: Int -> (Tree23 Int) -> [Int] -> Int
    solve accum _ [] = accum
    solve accum t (x:xs) = solve (accum + countGt t x) (insert t x) xs


readTestCase :: IO [Int]
readTestCase = do
    _ <- getLine
    line <- getLine
    return (map read $ words line)

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTestCase testCases
    mapM_ print ans