#include <iostream>
#include <algorithm>
using namespace std;

int n, p, q;
int a[100];

struct Solution{
    int m, diff;
};

bool is_better(const Solution &s1, const Solution &s2){
    if (s1.diff == s2.diff){
        return s1.m < s2.m;
    }
    return s1.diff > s2.diff;
}

int diff_around(int num){

    if (a[0] >= num){
        return a[0] - num;
    }
    if (a[n - 1] <= num) {
        return num - a[n - 1];
    }
    int i = 0;
    while (a[i] < num) i++;
    return min(num - a[i - 1], a[i] - num);
}

Solution solution_around(int num){
    Solution sol;
    sol.m = num;
    sol.diff = diff_around(num);
    return sol;
}

int main(){

    cin >> n;
    for (int i = 0; i < n; i++){
        cin >> a[i];
    }

    cin >> p >> q;

    sort(a, a + n);

    Solution first = solution_around(p);
    Solution last = solution_around(q);
    Solution best = is_better(last, first) ? last : first;

    for (int i = 1; i < n; i++){

        Solution cur;
        cur.diff = (a[i] - a[i - 1]) / 2;
        cur.m = a[i - 1] + cur.diff;

        if (p <= cur.m and  cur.m <= q and is_better(cur, best)){
            best = cur;
        }
    }

    cout << best.m << endl;

}