import Control.Monad (replicateM)
import Data.Vector (fromList, (!))


quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:[]) = [x]
quicksort (x:xs) = quicksort lesser ++ [x] ++ quicksort greater
    where
    lesser = filter (<= x) xs
    greater = filter (> x) xs


solve :: [Integer] -> Int -> Integer
solve ns k = minimum $ map unfairnessAt [0..(length ns - k)]
    where
    unfairnessAt i = (sortedVector ! j) - (sortedVector ! i)
        where
        j = i + k - 1
        sortedVector = fromList $ quicksort ns


main :: IO ()
main = do
    n <- readLn
    k <- readLn
    ns <- replicateM n readLn
    let
        ans = solve ns k
    print ans