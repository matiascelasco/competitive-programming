from collections import deque


class Node(object):

    def __init__(self, name, time):
        self.time = float(time)
        self.expected_time_partial = None
        self.expected_time = None
        self.adj = []
        self.degree = 0
        self.name = name
        self.parent = None
        self.num_children = 0

    def add_adj(self, other):
        self.adj.append(other)
        self.degree += 1

    def children(self):
        for node in self.adj:
            if node != self.parent:
                yield node

    def siblings(self):
        for node in self.parent.adj:
            if node != self:
                yield node


def avg(values):
    count = 0
    total = 0
    for value in values:
        count += 1
        total += value
    if count == 0:
        return 0
    return total / count


def read_line():
    line = raw_input()
    for word in line.split(' '):
        try:
            yield int(word)
        except ValueError:
            pass

n, = read_line()
nodes = [Node(i + 1, num) for i, num in enumerate(read_line())]

for _ in range(n - 1):
    a, b = read_line()
    a -= 1
    b -= 1
    nodes[a].add_adj(nodes[b])
    nodes[b].add_adj(nodes[a])

leaves = deque(n for n in nodes if n.degree == 1)

while len(leaves) > 1:
    leaf = leaves.popleft()
    leaf.parent = (node for node in leaf.adj if node.parent != leaf).next()
    leaf.parent.num_children += 1
    leaf.expected_time_partial = leaf.time + avg(n.expected_time_partial for n in leaf.children())
    if len(leaf.parent.adj) - leaf.parent.num_children == 1:
        leaves.append(leaf.parent)

root = leaves.popleft()
root.expected_time = root.time + avg(n.expected_time_partial for n in root.adj)

stack = [node for node in root.children()]

while stack:
    cur = stack.pop()
    # print 'cur', cur.name
    adj_expected_times = [node.expected_time_partial for node in cur.children()]

    # first we calculate the expected time after reading the parent
    parent_expected_time = cur.parent.expected_time - cur.parent.time
    # then we substract the partial calculation corresponding to the current node
    parent_expected_time -= cur.expected_time_partial / len(cur.parent.adj)
    # then we adjust the value since, for the parent, the childrens average now doesn't includes the
    # current node
    parent_expected_time *= len(cur.parent.adj) / (len(cur.parent.adj) - 1)
    # we add the parent time back
    parent_expected_time += cur.parent.time

    adj_expected_times.append(parent_expected_time)
    # print "adj_expected_times", adj_expected_times
    cur.expected_time = cur.time + avg(adj_expected_times)
    # print 'cur.expected_time', cur.expected_time
    for child in cur.children():
        stack.append(child)

print min((node.expected_time, node.name) for node in nodes)[1]

min_time = nodes[0].expected_time
ans = nodes[0].name

for node in nodes:
    # print node.name, "%.3f" % node.expected_time, node.expected_time_partial
    if node.expected_time < min_time:
        min_time = node.expected_time
        ans = node.name
