import Data.List (intercalate)

pairMap :: (t1 -> t2) -> (t1, t1) -> (t2, t2)
pairMap f (x, y) = (f x, f y)

isKaprekar :: Int -> Bool
isKaprekar x = l + r == x
    where
    (l, r) = pairMap read $ pairMap fix $ splitAt (length squareStr `div` 2) squareStr
    squareStr = show (x * x)
    fix strNum = if strNum == "" then "0" else strNum

kaprekarsInRange :: (Int, Int) -> [Int]
kaprekarsInRange (from, to) = filter isKaprekar [from..to]


main :: IO ()
main = do
    from <- readLn
    to <- readLn
    let
        ansNums = kaprekarsInRange (from, to)
        ans | ansNums == [] = "INVALID RANGE"
            | otherwise = intercalate " " $ map show ansNums
    putStrLn ans
