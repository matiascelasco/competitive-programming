import Control.Monad (replicateM, mapM_)
import Data.List (sort, intercalate)


uniq :: Eq t => [t] -> [t]
uniq [] = []
uniq (x:xs) = x:(uniq $ dropWhile (== x) xs)

rmRepetitions :: (Eq t, Ord t) => [t] -> [t]
rmRepetitions = uniq . sort

readTestCase :: IO (Int, Int, Int)
readTestCase = do
    numsList <- replicateM 3 getLine
    n <- return $ numsList !! 0
    a <- return $ numsList !! 1
    b <- return $ numsList !! 2
    return (read n, read a, read b)

solveTest :: (Int, Int, Int) -> String
solveTest (n, a, b) = intercalate " " $ map show (rmRepetitions possibleValues)
    where
    possibleValues = map (\x -> x * a + (n - 1 - x) * b) [0..(n - 1)]


main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTest testCases
    mapM_ putStrLn ans
