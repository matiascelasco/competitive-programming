from copy import copy

def perm(nums, i, out):
    if i >= len(nums) - 1:
        out.append(copy(nums))
        return
    swap_with = i
    while swap_with < len(nums):
        nums[i], nums[swap_with] = nums[swap_with], nums[i]
        perm(nums, i + 1, out)
        swap_with += 1
        while swap_with < len(nums) and nums[swap_with] == nums[i]:
            swap_with += 1

    cur = i + 1
    while cur < len(nums):
        nums[cur - 1], nums[cur] = nums[cur], nums[cur - 1]
        cur += 1


class Solution:
    # @param {integer[]} nums
    # @return {integer[][]}
    def permuteUnique(self, nums):
        nums.sort()
        ans = []
        perm(nums, 0, ans)
        return ans
