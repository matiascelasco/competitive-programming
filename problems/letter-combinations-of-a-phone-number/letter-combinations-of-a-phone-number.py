def comb(ds, m, i, prefix):
    if i == len(ds):
        yield prefix
        return
    for c in m[ds[i]]:
        combinations = comb(ds, m, i + 1, prefix + c)
        for combination in combinations:
            yield combination


class Solution:
    # @param {string} digits
    # @return {string[]}
    def letterCombinations(self, digits):
        letters_map = {
            '0': '',
            '1': '',
            '2': 'abc',
            '3': 'def',
            '4': 'ghi',
            '5': 'jkl',
            '6': 'mno',
            '7': 'pqrs',
            '8': 'tuv',
            '9': 'wxyz',
            '*': '+',
            '#': '',
        }
        return list(comb(digits, letters_map, 0, ''))


print Solution().letterCombinations(raw_input())
