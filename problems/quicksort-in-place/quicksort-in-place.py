n = input()
nums = [int(x) for x in raw_input().split(" ")]


def partition(nums, i, j):
    length = j - i
    if length <= 1:
        return

    lt = i
    pivote = nums[j - 1]
    for k in xrange(i, j - 1):
        if nums[k] <= pivote:
            nums[k], nums[lt] = nums[lt], nums[k]
            lt += 1

    nums[j - 1], nums[lt] = nums[lt], nums[j - 1]

    print ' '.join([str(x) for x in nums])

    partition(nums, i, lt)
    partition(nums, lt + 1, j)

partition(nums, 0, n)
