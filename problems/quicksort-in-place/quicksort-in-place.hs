module Main where
import Control.Monad (mapM_)
import Data.List (intercalate)
import Data.Vector as V (Vector, fromList)


swap :: (Vector t) -> Int -> Int -> (Vector t)
swap v i j =

solve :: [Int] -> [String]
solve n nums = map (intercalate " ") lists
    where
    vector = fromList nums
    lists = f vector 0 (n - 1)


main :: IO ()
main = do
    n <- readLn :: IO Int
    line <- getLine
    let
        nums = map read (words line)
        ans = solve n nums
    mapM_ putStrLn ans