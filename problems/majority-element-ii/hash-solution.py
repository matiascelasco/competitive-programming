from collections import Counter


def majority_elements(nums):
        counter = Counter()
        for num in nums:
            counter[num] += 1
        for num, count in counter.iteritems():
            if count > len(nums) / 3:
                yield num


class Solution:
    # @param {integer[]} nums
    # @return {integer[]}
    def majorityElement(self, nums):
        return list(majority_elements(nums))


print list(Solution().majorityElement(map(int, raw_input().split(' '))))
