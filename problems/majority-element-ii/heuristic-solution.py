import string


def normalize(num, max_len):
    s = str(num)
    sign = True
    if s[0] == '-':
        sign = False
        s = s[1:]
    return ('+' if sign else '-') + ((max_len - len(s)) * '0') + s


def index(symbol):
    if symbol == '+':
        return 10
    if symbol == '-':
        return 11
    return ord(symbol) - ord('0')


def is_candidate(num, position_candidates):
    return all(symbol in position_candidates[i] for i, symbol in enumerate(num))


def meet_condition(candidate, nums):
    return len([num for num in nums if num == candidate]) > len(nums) / 3


class Solution:
    # @param {integer[]} nums
    # @return {integer[]}
    def majorityElement(self, nums):
        if not nums:
            return []
        symbols = string.digits + '+-'
        max_len = max(len(str(num)) for num in nums)
        nums = [normalize(num, max_len) for num in nums]
        position_candidates = (max_len + 1) * [[]]
        for i in range(max_len + 1):
            counter = 12*[0]
            for num in nums:
                counter[index(num[i])] += 1
            for pos in range(12):
                if counter[pos] > len(nums) / 3:
                    position_candidates[i].append(symbols[pos])
        candidates = [num for num in nums if is_candidate(num, position_candidates)]
        return sorted(int(candidate) for candidate in set(candidates) if meet_condition(candidate, nums))

print Solution().majorityElement(raw_input().split(' '))
