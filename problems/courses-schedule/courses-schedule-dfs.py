class Node(object):

    def __init__(self, num):
        self.num = num
        self.adj = []

    def add_adj(self, other):
        self.adj.append(other)


def has_cycles(nodes):

    visited = [None for __ in nodes]

    def dfs(node, label):
        if visited[node.num] == label:
            return True
        visited[node.num] = label

        for a in node.adj:
            if visited[a.num] == label:
                return True
        for a in node.adj:
            if visited[a.num] is None and dfs(a, label):
                return True
        return False

    label = 0
    for node in nodes:
        if visited[node.num] is None:
            if dfs(node, label):
                return True
            label += 1
    return False


class Solution(object):
    # @param {integer} numCourses
    # @param {integer[][]} prerequisites
    # @return {boolean}
    def canFinish(self, numCourses, prerequisites):
        nodes = [Node(i) for i in xrange(numCourses)]
        for i, j in prerequisites:
            nodes[i].add_adj(nodes[j])
        return not has_cycles(nodes)
