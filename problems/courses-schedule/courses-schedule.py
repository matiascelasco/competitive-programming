class Node(object):

    def __init__(self, num):
        self.num = num
        self.in_deg = 0
        self.adj = []

    def add_adj(self, other):
        other.in_deg += 1
        self.adj.append(other)


def topological_sort(nodes):
    in_deg = [n.in_deg for n in nodes]
    stack = [n for n in nodes if in_deg[n.num] == 0]
    while stack:
        node = stack.pop()
        yield node
        for a in node.adj:
            in_deg[a.num] -= 1
            if in_deg[a.num] == 0:
                stack.append(a)


class Solution(object):
    # @param {integer} numCourses
    # @param {integer[][]} prerequisites
    # @return {boolean}
    def canFinish(self, numCourses, prerequisites):
        nodes = [Node(i) for i in xrange(numCourses)]
        for i, j in prerequisites:
            nodes[i].add_adj(nodes[j])
        ts = topological_sort(nodes)
        finished_courses = sum(1 for i in ts)
        return finished_courses == numCourses

