#include <iostream>
#include <string>
#include <sstream>
using namespace std;

typedef unsigned int uint;

struct Tree {
    virtual uint height() const = 0;
    virtual uint size() const = 0;
    virtual string to_string() const = 0;
};

struct TreeRoot : Tree {

    int value;
    Tree *left;
    Tree *right;

    TreeRoot(int value, Tree *left, Tree *right){
        this->value = value;
        this->left = left;
        this->right = right;
    }

    uint height() const {
        return 1 + max(left->height(), right->height());
    }

    uint size() const {
        return 1 + left->size() + right->size();
    }

    string to_string() const {
        ostringstream stream;
        stream << "(" << left->to_string() << ")" << value << "(" << right->to_string() << ")";
        return stream.str();
    }

};

struct EmptyTree : Tree {

    uint height() const {
        return 0;
    }

    uint size() const {
        return 0;
    }

    string to_string() const {
        return "*";
    }

};

ostream& operator<<(ostream &o, const Tree &tree){
    o << tree.to_string();
    return o;
}

bool is_digit(char c){
    if (c == '*'){
        return true;
    }
    return '0' <= c and c <= '9';
}

void print_by_levels(Tree &tree){

    string str = tree.to_string();

    for (uint level = 0; level <= tree.height(); level++){

        ostringstream stream;

        uint current_level = 0;
        for (uint i = 0; i < str.size(); ++i){
            if (str[i] == '('){
                current_level++;
            }
            else if (str[i] == ')'){
                current_level--;
            }
            else if (current_level == level){
                stream << str[i];
            }
            else {
                stream << ' ';
            }
        }
        if (level > 0){
            string s = stream.str();
            bool left = true;
            for (uint i = 0; i < s.size(); ++i){
                if (s[i] != ' '){
                    s[i] = left ? '/' : '\\';
                }
                if (s[i] == ' ' and i > 0 and s[i - 1] != ' '){
                    left = not left;
                }
            }

            for (uint i = 0; i < s.size() - 1; i++){
                if (s[i] == '/' and s[i + 1] == '/'){
                    s[i] = ' ';
                }
            }
            for (uint i = s.size() - 1; i > 0; i--){
                if (s[i] == '\\' and s[i - 1] == '\\'){
                    s[i] = ' ';
                }
            }
            cout << s << endl;
        }
        cout << stream.str() << endl;
    }

}


Tree* build_tree(int array[], uint size){
    if (size == 0){
        return new EmptyTree();
    }
    uint mid = size - size / 2 - 1;

    return new TreeRoot(
        array[mid],
        build_tree(array, mid),
        build_tree(array + mid + 1, size - mid - 1)
    );
}

int main(){

    uint size = 12;

    int array[size];

    for (uint i = 0; i < size; ++i){
        array[i] = i;
    }

    Tree *n = build_tree(array, size);

    print_by_levels(*n);

    return 0;
}