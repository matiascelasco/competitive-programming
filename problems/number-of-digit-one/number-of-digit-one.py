def digits(n):
    while (n > 0):
        yield n % 10
        n /= 10


def count(gen):
    c = 0
    for x in gen:
        c += 1
    return c


def count_ones(n):
    return count(d for d in digits(n) if d == 1)


def all_nines(n):
    return all(d == 9 for d in digits(n))


def count_from_zero(n):

    if n <= 0:
        # print "zero", n, 0
        return 0

    if n <= 9:
        # print "single_digit", n, 1
        return 1

    if all_nines(n):
        ans = 10 * count_from_zero(n / 10) + (n + 1) / 10
        # print "all_nines", n, ans
        return ans

    reminder = n % 10
    first_part = n / 10
    on_last_digit = first_part + 1 if reminder >= 1 else 0
    on_the_first_digits = (count_ones(first_part) * (reminder + 1) +
                           count_from_zero(first_part - 1) * 10)
    return on_last_digit + on_the_first_digits


class Solution:
    # @param {integer} n
    # @return {integer}
    def countDigitOne(self, n):
        return count_from_zero(n)

print Solution().countDigitOne(input())
