#include <iostream>
using namespace std;

int main(){
    int asc = 0;
    int desc = 0;
    int n, x;
    cin >> n;
    for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++){
        cin >> x;
        if (i == j){
            desc += x;
        }
        if (n - j - 1 == i){
            asc += x;
        }
    }
    cout << abs(asc - desc) << endl;
}