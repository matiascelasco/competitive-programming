def operate(o1, op, o2):
    if op == '+':
        return o1 + o2
    if op == '-':
        return o1 - o2
    if op == '*':
        return o1 * o2
    if op == '/':
        if (o1 < 0) != (o2 < 0):
            return - (o1 / (-o2))  # because Python has floor division
        else:
            return o1 / o2


class Solution:
    # @param {string[]} tokens
    # @return {integer}
    def evalRPN(self, tokens):
        stack = []
        for t in tokens:
            if t in '+-*/':
                o2 = stack.pop()
                o1 = stack.pop()
                stack.append(operate(o1, t, o2))
            else:
                stack.append(int(t))
        return stack[0]


print Solution().evalRPN(["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"])
