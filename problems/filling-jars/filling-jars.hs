module Main where
import Control.Monad (replicateM)

solve :: Int -> [(Integer, Integer, Integer)] -> Integer
solve n ops = floor ((fromIntegral total) / (fromIntegral n) :: Double)
    where
    total = sum $ map (\(a, b, k) -> (b - a + 1) * k) ops

readOperation :: IO (Integer, Integer, Integer)
readOperation = do
    line <- getLine
    let
        [a, b, k] = map read (words line)
    return (a, b, k)

main :: IO ()
main = do
    firstLine <- getLine
    let
        [n, m] = map read (words firstLine)
    operations <- replicateM m readOperation
    let
        ans = solve n operations
    print ans