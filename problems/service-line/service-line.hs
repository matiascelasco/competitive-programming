import Control.Monad (return, replicateM, mapM_)
import Data.List (foldl')

line_values = (map read) . words

slice i j =  take (j - i + 1) . drop i

myMinimum (x:xs) = foldl' min x xs

solve_test widths values = min 3 $ myMinimum $ slice i j widths
    where i = values !! 0
          j = values !! 1

main :: IO ()
main = do
  first_line <- getLine :: IO String
  first_line_values <- return $ line_values first_line
  n <- return $ first_line_values !! 0
  t <- return $ first_line_values !! 1
  second_line <- getLine :: IO String
  last_t_lines <- replicateM t getLine
  let 
    widths = line_values second_line
    tests = map line_values last_t_lines
    ans = map (solve_test widths) tests
  mapM_ print ans
  