import Control.Monad (replicateM)

data Person = Person Int String

instance Eq Person where
    (Person id1 _) == (Person id2 _) = id1 == id2
instance Ord Person where
    (Person id1 _) < (Person id2 _) = id1 < id2

instance Show Person where
    show (Person int str) = show (int, str)

getBitSequence :: Person -> String
getBitSequence (Person _ str) = str

pairMap :: (t1 -> t2) -> (t1, t1) -> (t2, t2)
pairMap f (x, y) = (f x, f y)

onesCount :: String -> Int
onesCount = length . filter (== '1')

pairStrOr :: (String, String) -> String
pairStrOr (x, y) = map (\(a, b) -> if (a == '1') || (b == '1') then '1' else '0') (zip x y)

cartProd :: [t] -> [t] -> [(t, t)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

cartSquare :: [t] -> [(t, t)]
cartSquare list = cartProd list list

solve :: [String] -> (Int, Int)
solve bitSequences = (maxVal, numberOfTeams)
    where
    numberOfTeams = length $ filter (== maxVal) values
    maxVal = maximum values
    values = map (onesCount . pairStrOr) pairs
    pairs = map (pairMap getBitSequence) $ filter (\(x, y) -> x < y) $ cartSquare persons
    persons = map (\(int, str) -> Person int str) (zip [1..] bitSequences)

printPair :: Show t => (t, t) -> IO ()
printPair (x, y) = mapM_ print [x, y]

getPairOfIntsFromLine :: IO (Int, Int)
getPairOfIntsFromLine = do
    line <- getLine
    let
        asWords = map read $ words line
        x = asWords !! 0
        y = asWords !! 1
    return (x, y)

main :: IO ()
main = do
    (n, _) <- getPairOfIntsFromLine
    bitSequences <- replicateM n getLine
    let
        ans = solve bitSequences
    printPair ans

