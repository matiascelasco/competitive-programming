import Control.Monad (replicateM, mapM_)


solveTest :: Int -> Int
solveTest n = length $ filter ((== 0) . (n `mod`)) (filter (> 0) digits)
    where
    digits :: [Int]
    digits = extractDigit [] n
        where
        extractDigit :: [Int] -> Int -> [Int]
        extractDigit ds 0 = ds
        extractDigit ds n = extractDigit ((n `mod` 10):ds) (n `div` 10)


main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readLn
    let
        ans = map solveTest testCases
    mapM_ print ans