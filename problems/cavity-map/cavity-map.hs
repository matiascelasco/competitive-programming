import Control.Monad (replicateM, mapM_)
import Data.Vector (Vector, fromList, (!))
import qualified Data.Vector (length)
type Matrix t = Vector (Vector t)

fromListOfLists :: [[t]] -> Matrix t
fromListOfLists = fromList . (map fromList)

readMatrix :: Int -> IO (Matrix Char)
readMatrix n = do
    rows <- replicateM n getLine
    let
        matrix = fromListOfLists rows
    return matrix

cartProd :: [t] -> [t] -> [(t, t)]
cartProd xs ys = [(x, y) | x <- xs, y <- ys]

cartSquare :: [t] -> [(t, t)]
cartSquare xs = cartProd xs xs

asListOfLists :: Int -> [t] -> [[t]]
asListOfLists n xs = rec xs
    where
    rec [] = []
    rec list = (row:rec rest)
        where
        (row, rest) = splitAt n list

solve :: Matrix Char -> [[Char]]
solve m = asListOfLists n $ map (\(i, j) -> if isCavity (i, j) then 'X' else (m ! i) ! j) positions
    where
    positions = cartSquare [0..(n - 1)]
    n = (Data.Vector.length m)
    isCavity (i, j) = not inBorder && cur > (maximum [left, top, right, bottom])
        where
        inBorder = i == 0 || j == 0 || i == (n - 1) || j == (n - 1)
        cur = (m ! i) ! j
        left = (m ! (i - 1)) ! j
        top = (m ! i) ! (j - 1)
        right = (m ! (i + 1)) ! j
        bottom = (m ! i) ! (j + 1)

main :: IO ()
main = do
    n <- readLn
    m <- readMatrix n
    let
        ans = solve m
    mapM_ putStrLn ans