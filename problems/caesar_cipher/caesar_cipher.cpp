#include <iostream>
#include <string>
using namespace std;

int k;

void maybe_convert(char &c, char first, char last){
    if (c < first or c > last){
        return;
    }
    int ord = c - first;
    ord += k;
    ord %= last - first + 1;
    c = first + ord;
}

int main(){
    string s;
    cin >> k;
    cin >> s;
    cin >> k;

    for (unsigned int i = 0; i < s.size(); i++){
        maybe_convert(s[i], 'a', 'z');
        maybe_convert(s[i], 'A', 'Z');
    }
    cout << s << endl;
}