input()
s = raw_input()
k = input()


def maybe_convert_in_range(c, fst, lst):
    if c < fst or c > lst:
        return c
    o = ord(c) - ord(fst)
    o += k
    o %= ord(lst) - ord(fst) + 1
    o += ord(fst)
    return chr(o)


def convert(c):
    c = maybe_convert_in_range(c, 'a', 'z')
    c = maybe_convert_in_range(c, 'A', 'Z')
    return c

s = ''.join(map(convert, s))
print s
