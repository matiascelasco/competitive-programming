from collections import Counter

s = raw_input()

counter = Counter()
for c in s:
    counter[c] += 1

odd_freq_counter = 0
for k, v in counter.iteritems():
    if v % 2 == 1:
        odd_freq_counter += 1

print "YES" if odd_freq_counter < 2 else "NO"
