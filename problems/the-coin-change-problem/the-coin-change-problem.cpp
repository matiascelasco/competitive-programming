#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

typedef unsigned long long int llint;

int n, m, c[50];
bool mem_check[251][50];
llint mem[251][50];

llint f(int n, int from){
    if (n == 0){
        return 1;
    }
    if (mem_check[n][from]){
        return mem[n][from];
    }
    llint accum = 0;
    for (int i = from; i < m; i++){
        for (int j = 1; j * c[i] <= n; j++){
            accum += f(n - j * c[i], i + 1);
        }
    }
    mem_check[n][from] = true;
    return mem[n][from] = accum;
}

int main() {

    cin >> n >> m;
    for (int i = 0; i <= n; i++){
        for (int j = 0; j < m; j++){
            mem_check[i][j] = false;
        }
    }
    for (int i = 0; i < m; ++i) {
        cin >> c[i];
    }
    sort(c, c + m, greater<int>());
    cout << f(n, 0) << endl;

    return 0;
}