#include <iostream>
using namespace std;

int n;
int a[1000000];

int main(){

    cin >> n;

    int total_xor = 0;
    for (int i = 0; i < n; i++){
        cin >> a[i];
        total_xor xor_eq a[i];
    }

    int ans = 0;
    for (int i = 0; i < n; i++){
        if (a[i] > (a[i] xor total_xor)){
            ans++;
        }
    }

    cout << ans << endl;

    return 0;
}