def is_valid(s):
    return int(s) <= 255 and (s == '0' or s[0] != '0')


def restore(prefix, s, n):
    if len(s) < n or len(s) > n*3:
        return []
    if n == 1:
        return [prefix + s] if is_valid(s) else []
    ans = []
    for i in (1, 2, 3):
        num, remainder = s[:i], s[i:]
        if is_valid(num):
            ans += restore(prefix + num + '.', remainder, n - 1)
    return ans
