import Control.Monad (replicateM, mapM_)


solveTest :: (Int, Int, Int) -> Int
solveTest (n, c, m) = r (n `div` c) (n `div` c)
    where
    r eaten wrappers | wrappers < m = eaten
                     | otherwise =  r (wrappers `div` m + eaten) (wrappers `div` m + wrappers `mod` m)


tuplify3 :: [t] -> (t, t, t)
tuplify3 list = (x, y, z)
    where
    x = list !! 0
    y = list !! 1
    z = list !! 2

main :: IO ()
main = do
    t <- readLn
    testCaselines <- replicateM t getLine
    let
        testCases = map (tuplify3 . map read . words) testCaselines
        ans = map solveTest testCases
    mapM_ print ans