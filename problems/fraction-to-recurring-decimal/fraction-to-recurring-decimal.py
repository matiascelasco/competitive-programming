from __future__ import division
from fractions import Fraction


def simplify_fraction(n, d):
    f = Fraction(n, d)
    return f.numerator, f.denominator


class Solution(object):
    def fractionToDecimal(self, n, d):
        """
        :type numerator: int
        :type denominator: int
        :rtype: str
        """
        n, d = simplify_fraction(n, d)
        sign = 1 if (n < 0) == (d < 0) else -1
        n, d = abs(n), abs(d)
        int_part = str(n // d)
        n = n % d * 10
        res = []
        used = dict()
        periodic = None
        while True:
            c = n // d
            r = n % d
            used[n] = len(res)
            res.append(c)
            # print "n =", n
            # print "d =", d
            # print "c =", c
            # print "r =", r
            # print "used =", used
            # print "res =", res
            if r == 0:
                break
            n = r * 10
            if n in used:
                periodic = used[n]
                break
        dec_part = ''.join(map(str, res[:periodic]))
        if periodic is not None:
            per_part = ''.join(map(str, res[periodic:]))
            dec_part = "%s(%s)" % (dec_part, per_part)
        int_part = ('-' if sign == (-1) else '') + int_part
        if dec_part == '0':
            return int_part
        return int_part + ('.' + dec_part if dec_part else '')

n, d = map(int, (raw_input().split()))
print Solution().fractionToDecimal(n, d)
