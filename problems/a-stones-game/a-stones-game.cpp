#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int power_of_two[] = {
    1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
    2048, 4096, 8192, 16384, 32768, 65536, 131072,
    262144, 524288, 1048576
};

int grundy(int num){
    return floor(log2(num)) + 1;
}

int calculate(int n){
    int total_xor = grundy(n) xor 1;
    int min = n;
    for (int i = 1, g = 1; i <= n; i*=2, g++){
        int target = g xor total_xor;
        if (target < g){
            int last_with_target_grundy = power_of_two[target] - 1;
            int candidate = i - last_with_target_grundy;
            if (candidate < i / 2){
                candidate = i - i / 2;
            }
            if (candidate < min){
                min = candidate;
            }
        }
    }
    return min;
}

int main(){
    int t, n;
    cin >> t;
    while(t--){
        cin >> n;
        if (n % 2){  //odd
            cout << 1 << endl;
        }
        else {  //even
            cout << calculate(n) << endl;
        }
    }
    return 0;
}