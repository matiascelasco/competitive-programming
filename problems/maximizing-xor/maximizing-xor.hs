import Data.Bits (xor)

logBase2Int :: Int -> Int
logBase2Int = floor . logBase 2 . fromIntegral


maxXor :: Int -> Int -> Int
maxXor l r = 2 ^ (logBase2Int (l `xor` r) + 1) - 1 

main :: IO ()
main = do
  l <- readLn
  r <- readLn
  print $ maxXor l r
