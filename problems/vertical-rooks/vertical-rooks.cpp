#include <iostream>
#include <cmath>
using namespace std;

int t, n, x;
int grundy[2000];

int main(){

    cin >> t;
    while (t--){
        cin >> n;
        for (int i = 0; i < n; i++){
            cin >> grundy[i];
        }
        for (int i = 0; i < n; i++){
            cin >> x;
            grundy[i] = abs(grundy[i] - x) - 1;
        }
        int total_xor = 0;
        for (int i = 0; i < n; i++){
            total_xor = total_xor xor grundy[i];
        }
        cout << "player-" << (total_xor ? 2 : 1) << endl;
    }

    return 0;
}