"""

My solution calculates both sequences of counts of non decreasing and not
increasing ranges separately, and then prints the substraction.

Both count sequences are calculated with the same generic function, which
receives a comparator function as an argument.

The key idea of the algorithm is the following: given any sorted sequence
each subrange within its boundaries will be sorted with the same criteria
too.
This way, given a non decreasing (or non increasing) sequence of S
elements, we know that it contains S * (S - 3) / 2 different ranges of
non decreasing (or non increasing) numbers. Or S * (S - 1) / 2 if we
consider ranges with a single element as valid, but the statement defines
ranges as [a, b] where a < b. However, the result is the same anyway since
both counts are finally subscracted for the final result and single element
ranges are non decreasing and non increasing at the same time.

So, since any sequence can be partitioned in a sequence of non decreasing
(or non increasing) subsequences, we can consider the size of each of this
partitions, calculate the number of non decreasing (or non increasing)
subranges between the boundaries of each one and sum them all.

This could be done manually for each window, but it would be too slow
(cuadratic). Instead of that, I just calcualte the count for the first
window and the following counts are calculated in constant time by
updating the first and last partition of the current window, which is equal
to the previous one except of the element on both ends.

Since I need efficent access to both ends, I use a deque to keep track of
the sizes of the partitions.

"""


from collections import deque
from operator import le, ge


def partition_sizes(xs, k, cmp):

    count = 1

    for i in range(1, k):
        if cmp(xs[i - 1], xs[i]):
            count += 1
        else:
            yield count
            count = 1

    yield count


def calc(xs, k, cmp):

    sizes = deque(partition_sizes(xs, k, cmp))
    count = sum(size * (size - 3) / 2 for size in sizes)
    yield count + 1

    for last in xrange(k, len(xs)):

        count -= sizes[0]
        sizes[0] -= 1
        if sizes[0] == 0:
            sizes.popleft()

        if not cmp(xs[last - 1], xs[last]):
            sizes.append(0)
        sizes[-1] += 1
        count += sizes[-1]

        yield count + 1


def read_numbers():
    line = raw_input()
    for word in line.split(' '):
        try:
            yield int(word)
        except ValueError:
            # just in case there are blank spaces at the begining of the line
            # or at the end of the line, or more than one blank spaces between
            # numbers
            pass

n, k = read_numbers()
xs = list(read_numbers())

assert len(xs) == n

if k == 1:
    for _ in xrange(n):
        print 0
else:
    non_dec_count = calc(xs, k, le)
    non_inc_count = calc(xs, k, ge)

    for a, b in zip(non_dec_count, non_inc_count):
        print a - b
