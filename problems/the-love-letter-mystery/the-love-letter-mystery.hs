import Control.Monad (replicateM, mapM_)
import Data.Char (ord)

solve_test string = sum $ map (\(x, y) -> abs (x - y)) $ zip first_half_ords (reverse last_half_ords)
    where len = length string
          first_half_ords = take (len `div` 2) ords
          last_half_ords = drop (len - (len `div` 2)) ords
          ords = map ord string

main :: IO ()
main = do
    t <- readLn :: IO Int
    tests <- replicateM t getLine
    let
        ans = map solve_test tests
    mapM_ print ans
