# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None


def reverse_prefix(head, size):
    if size == 1:
        return head
    cur = head
    prev = None
    cur_size = 1
    while cur_size <= size:
        new_cur = cur.next
        cur.next = prev
        prev = cur
        cur = new_cur
        cur_size += 1
    head.next = cur
    return prev


def find(head, pos):
    cur = head
    cur_pos = 1
    while cur_pos < pos:
        cur = cur.next
        cur_pos += 1
    return cur


class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        size = n - m + 1
        if m == 1:
            return reverse_prefix(head, size)
        before = find(head, m - 1)
        m_node = before.next
        before.next = None
        m_node = reverse_prefix(m_node, size)
        before.next = m_node
        return head