import Control.Monad (mapM_, replicateM)


solveTestCase :: (Int, Int, [Int]) -> String
solveTestCase (n, k, xs) = if willCancell then "YES" else "NO"
    where willCancell = length punctualStudents < k
          punctualStudents = filter (<= 0) xs


main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTestCase testCases
    mapM_ putStrLn ans


readTestCase :: IO (Int, Int, [Int])
readTestCase = do
    firstLine <- getLine
    secondLine <- getLine
    let
        firstLineParsed = map read $ words firstLine
        n = firstLineParsed !! 0
        k = firstLineParsed !! 1
        xs = map read $ words secondLine
    return (n, k, xs)