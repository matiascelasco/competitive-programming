solve :: Int -> [Int] -> Int
solve = solve' 0
    where
    solve' _ _ [] = error "Not found"
    solve' i x (y:ys) | x == y = i
                      | otherwise = solve' (i+1) x ys

readNumList :: IO [Int]
readNumList = do
    line <- getLine
    return (map read (words line))

main :: IO ()
main = do
    x <- readLn
    _ <- readLn :: IO Int
    nums <- readNumList
    let
        ans = solve x nums
    print ans