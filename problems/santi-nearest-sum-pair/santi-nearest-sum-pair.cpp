/*
Given an integer N and an array A of integers, find the positions in A of a pair of
integers such as his sum is the nearest to N
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;


int n, size;

int diff(const vector<int> &v, const pair<int, int> &p){
    return abs(v[p.first] + v[p.second] - n);
}

int diff(const vector<int> &v, int i, int j){
    return diff(v, make_pair(i, j));
}

int find_best_for(const vector<int> &v, int i){
    int j = lower_bound(v.begin() + i + 1, v.end(), n - v[i]) - v.begin();

    // it means that all elements are lesser than the target,
    // so the position of the greatest is returned
    if (j == (int) v.size()){
        return j - 1;
    }

    if (j - 1 != i and diff(v, i, j - 1) < diff(v, i, j)){
        return j - 1;
    }
    return j;
}

int main(){

    cin >> n;
    cin >> size;
    vector<int> v(size);
    for (int i = 0; i < size; ++i){
        cin >> v[i];
    }

    sort(v.begin(), v.end());
    pair<int, int> best = make_pair(0, find_best_for(v, 0));
    for (int i = 1; i < size; ++i){
        pair<int, int> cur = make_pair(i, find_best_for(v, i));
        if (diff(v, cur) < diff(v, best)){
            best = cur;
        }
    }

    cout << v[best.first] << " " << v[best.second] << endl;

    return 0;
}