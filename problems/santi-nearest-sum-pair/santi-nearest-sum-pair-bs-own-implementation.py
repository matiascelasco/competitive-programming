n = input()
size = input()
v = map(int, raw_input().split(' '))

assert len(v) == size

v.sort()


def diff(i, j):
    return abs(v[i] + v[j] - n)


def bs(v, lo, hi, target):
    size = hi - lo
    mid = lo + size / 2

    if size < 2:
        return lo

    if v[mid] == target:
        return mid

    if v[mid] < target:
        return bs(v, mid, hi, target)
    else:
        return bs(v, lo, mid, target)


def best_for(i):
    j = bs(v, i + 1, len(v), n - v[i])

    if j == len(v):
        return j - 1

    if j - 1 != i and diff(i, j - 1) < diff(i, j):
        return j - 1

    return j


best = (0, best_for(0))
for i in range(1, size):
    cur = (i, best_for(i))
    if diff(*cur) < diff(*best):
        best = cur

print v[best[0]], v[best[1]]
