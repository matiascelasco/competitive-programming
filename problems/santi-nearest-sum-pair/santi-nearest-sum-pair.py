from bisect import bisect_left as lower_bound

n = input()
size = input()
v = map(int, raw_input().split(' '))

assert len(v) == size

v.sort()


def diff(i, j):
    return abs(v[i] + v[j] - n)


def best_for(i):
    j = lower_bound(v, n - v[i], lo=i + 1)

    if j == len(v):
        return j - 1

    if j - 1 != i and diff(i, j - 1) < diff(i, j):
        return j - 1

    return j


best = (0, best_for(0))
for i in range(1, size):
    cur = (i, best_for(i))
    if diff(*cur) < diff(*best):
        best = cur

print v[best[0]], v[best[1]]
