marked = set()


def mark(node):
    marked.add(node)


def is_marked(node):
    return node in marked


def find_and_mark(root, target):
    if root is None:
        return False
    if root is target:
        mark(root)
        return True
    if find_and_mark(root.left, target) or find_and_mark(root.right, target):
        mark(root)
        return True
    return False


def find_lca(root, p, q):
    for node in (p, q):
        if root is node:
            return node
    if is_marked(root.left) and is_marked(root.right):
        return root
    for child in (root.left, root.right):
        if is_marked(child):
            return find_lca(child, p, q)


def lowest_common_ancestor(root, p, q):
    find_and_mark(root, p)
    find_and_mark(root, q)
    return find_lca(root, p, q)
