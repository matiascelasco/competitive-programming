def lowest_common_ancestor(root, p, q):
    if p.val > q.val:
        p, q = q, p

    cur = root

    while True:
        for node in (p, q):
            if cur.val == node.val:
                return node
        if p.val < cur.val < q.val:
            return cur
        cur = cur.left if q.val < cur.val else cur.right
