def find_lca(root, p, q):
    assert p.val <= q.val
    for node in (p, q):
        if root.val == node.val:
            return node
    if p.val < root.val < q.val:
        return root
    assert q.val < root.val or root.val < p.val
    next_node = root.left if q.val < root.val else root.right
    return find_lca(next_node, p, q)


def lowest_common_ancestor(root, p, q):
    if p.val > q.val:
        p, q = q, p
    return find_lca(root, p, q)
