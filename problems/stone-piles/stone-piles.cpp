#include <iostream>
#include <set>
using namespace std;

typedef unsigned int uint;

uint test_cases;
uint num_piles;
uint pile_size;
uint total_xor;
uint nimber[51];

uint mex(const set<uint> &nums){
    uint ans = 0;
    while (nums.count(ans) > 0){
        ans++;
    }
    return ans;
}

void build_set(uint min_size, uint num_piles, uint accum_xor, set<uint> &next_nimbers){
    if (num_piles == 0){
        next_nimbers.insert(accum_xor);
        return;
    }

    for (uint i = min_size + 1; i <= num_piles; i++){
        build_set(
            i,
            num_piles - i,
            accum_xor xor nimber[i],
            next_nimbers
        );
    }
}

uint get_nimber(uint num_stones){
    set<uint> next_nimbers;
    for (uint i = 1; i < num_stones; i++){
        // i represents the size of the minor pile of stones
        build_set(i, num_stones - i, nimber[i], next_nimbers);
    }
    return mex(next_nimbers);
}

int main(){
    for (uint i = 0; i <= 50; i++){
        nimber[i] = get_nimber(i);
    }

    cin >> test_cases;
    while(test_cases--){
        cin >> num_piles;
        total_xor = 0;
        while(num_piles--){
            cin >> pile_size;
            total_xor = total_xor xor nimber[pile_size];
        }
        cout << (total_xor ? "ALICE" : "BOB") << endl;
    }

    return 0;
}