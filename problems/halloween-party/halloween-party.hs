import Control.Monad (replicateM, mapM_)


solveTest :: Int -> Int
solveTest k = half * (k - half) where half = k `div` 2

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readLn
    let
        ans = map solveTest testCases
    mapM_ print ans