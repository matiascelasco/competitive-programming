#include <iostream>
#include <utility>
using namespace std;

typedef long long int lint;

lint mem[50000][2];
int p[50000];
int t, n;

int max3(int a, int b, int c){
    return max(a, max(b, c));
}

lint f(int from, int shares = 0){

    if (from == n){
        return 0;
    }

    if (mem[from][shares] > -1){
        return mem[from][shares];
    }

    int price = p[from];

    lint profit_having_nothing_to_sell = f(from + 1, 0);
    lint profit_having_one_to_sell = f(from + 1, 1);

    lint ans = -1;


    for (int sold_shares = -1; sold_shares <= shares; sold_shares++){
        lint x = 0;
        x += profit_having_nothing_to_sell;
        x += (profit_having_one_to_sell - profit_having_nothing_to_sell) * (shares - sold_shares);
        x += price * sold_shares;
        ans = max(ans, x);
    }

    return mem[from][shares] = ans;
}

int main() {

    cin >> t;
    for (int i = 0; i < t; ++i){
        for (int j = 0; j < 50000; ++j){
            mem[j][0] = mem[j][1] = -1;
        }
        cin >> n;
        for (int j = 0; j < n; j++){
            cin >> p[j];
        }
        cout << f(0) << endl;
    }

    return 0;
}