import Data.List (sort)


getList :: IO [Int]
getList = do
    line <- getLine
    return $ map read (words line)


solve :: Int -> [Int] -> Int
solve k nums = solve' 1 (reverse $ sort nums)
    where
    solve' _ [] = 0
    solve' factor xs = sum (map (factor *) gretestKPrices) + solve' (factor + 1) lowestKPrices
        where
        (gretestKPrices, lowestKPrices) = splitAt k xs

main :: IO ()
main = do
    [_, k] <- getList
    nums <- getList
    let
        ans = solve k nums

    print ans