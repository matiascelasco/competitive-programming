class WordDictionary:
    # initialize your data structure here.
    def __init__(self):
        self.is_end = False
        self.children = dict()

    # @param {string} word
    # @return {void}
    # Adds a word into the data structure.
    def addWord(self, word):
        self.__add_word(word, 0)

    # @param {string} word
    # @return {boolean}
    # Returns if the word is in the data structure. A word could
    # contain the dot character '.' to represent any one letter.
    def search(self, word):
        return self.__search(word, 0)

    def __add_word(self, word, i):
        if i == len(word):
            self.is_end = True
            return
        if word[i] not in self.children:
            self.children[word[i]] = WordDictionary()
        self.children[word[i]].__add_word(word, i + 1)

    def __search(self, word, i):
        if i == len(word):
            return self.is_end

        if word[i] == '.':
            chs = self.children.itervalues()
        else:
            if word[i] in self.children:
                chs = [self.children[word[i]]]
            else:
                chs = []
        return any(n.__search(word, i + 1) for n in chs)
# Your WordDictionary object will be instantiated and called as such:
# wordDictionary = WordDictionary()
# wordDictionary.addWord("word")
# wordDictionary.search("pattern")
