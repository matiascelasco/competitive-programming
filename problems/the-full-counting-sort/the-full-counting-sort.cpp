#include <iostream>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

typedef unsigned int uint;

int main() {

    int n;
    cin >> n;

    vector< vector<string> > counter(100);

    for (int i = 0; i < n; ++i){
        int value;
        string str;
        cin >> value;
        cin >> str;
        if (i < n / 2){
            str = "-";
        }
        counter[value].push_back(str);
    }

    bool flag = false;
    for (int i = 0; i < 100; ++i){
        for (uint j = 0; j < counter[i].size(); j++){
            if (flag){
                cout << " ";
            }
            else {
                flag = true;
            }
            cout << counter[i][j];
        }
    }
    cout << endl;

    return 0;
}