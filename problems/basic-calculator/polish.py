class Token(object):

    def __init__(self, val):
        try:
            self.val = int(val)
        except ValueError:
            self.val = val

    def is_op(self):
        return self.val in '+-'

    def is_num(self):
        return type(self.val) is int


def should_ignore(c):
    return c == ' '


def is_digit(c):
    return ord('0') <= ord(c) <= ord('9')


def tokenizer(s):
    cur_int = []
    for c in s:
        if should_ignore(c):
            continue
        if is_digit(c):
            cur_int.append(c)
        else:
            if cur_int:
                yield Token(int(''.join(cur_int)))
                cur_int = []
            yield Token(c)
    if cur_int:
        yield Token(int(''.join(cur_int)))


def operate(o1, op, o2):
    # assert op.val in '+-'
    if op.val == '+':
        return Token(o1.val + o2.val)
    if op.val == '-':
        return Token(o1.val - o2.val)


def convert_to_inverse_polish(expr):
    op_stack = []
    for t in tokenizer(expr):
        if t.is_num():
            yield t
        elif t.is_op():
            while op_stack and op_stack[-1].is_op():
                yield op_stack.pop()
            op_stack.append(t)
        elif t.val == '(':
            op_stack.append(t)
        elif t.val == ')':
            while op_stack and op_stack[-1].is_op():
                yield op_stack.pop()
            # assert op_stack[-1].val == '('
            op_stack.pop()
    while op_stack and op_stack[-1].is_op():
        yield op_stack.pop()


def calc_inverse_polish(tokens):
    stack = []
    for t in tokens:
        if t.is_num():
            stack.append(t)
        else:
            # assert t.is_op()
            o2 = stack.pop()
            o1 = stack.pop()
            stack.append(operate(o1, t, o2))
    # assert len(stack) == 1
    return stack.pop().val


def calculate(expr):
    return calc_inverse_polish(convert_to_inverse_polish(expr))

print calculate(raw_input())
