FIRST_OPERAND = 1
SECOND_OPERAND = 2
OPERATOR = 0
CLOSE_PAR = 3


class Token(object):

    def __init__(self, val):
        try:
            self.val = int(val)
        except ValueError:
            self.val = val

    def is_op(self):
        return self.val in '+-'

    def is_num(self):
        return type(self.val) is int

    def __repr__(self):
        return "t{%s}" % str(self.val)


def should_ignore(c):
    return c == ' '


def is_digit(c):
    return ord('0') <= ord(c) <= ord('9')


def tokenizer(s):
    cur_int = []
    for c in s:
        if should_ignore(c):
            continue
        if is_digit(c):
            cur_int.append(c)
        else:
            if cur_int:
                yield Token(int(''.join(cur_int)))
                cur_int = []
            yield Token(c)
    if cur_int:
        yield Token(int(''.join(cur_int)))


def operate(o1, op, o2):
    assert op.val in '+-'
    if op.val == '+':
        return Token(o1.val + o2.val)
    if op.val == '-':
        return Token(o1.val - o2.val)


def can_operate(stack):
    return len(stack) >= 3 and stack[-1].is_num() and stack[-2].is_num() and stack[-3].is_op()


def can_undo_par(stack):
    return stack and stack[-1].val == ')'


def reduce_stack(stack):
    while can_operate(stack) or can_undo_par(stack):
        if can_operate(stack):
            # print "hora de reducir", stack
            second_operand = stack.pop()
            first_operand = stack.pop()
            operator = stack.pop()
            new_value = operate(first_operand, operator, second_operand)
            stack.append(new_value)
        else:
            count = 0
            while stack and stack[-1].val == ')':
                stack.pop()
                count += 1
            t = stack.pop()
            while count:
                assert stack[-1].val == '('
                stack.pop()
                count -= 1
            stack.append(t)


def calculate(s):
    looking_for = FIRST_OPERAND
    stack = []
    for t in tokenizer(s):
        reduce_stack(stack)
        if looking_for == FIRST_OPERAND:
            if t.is_num():
                stack.append(t)
                looking_for = OPERATOR
            else:
                assert t.val == '('
                stack.append(t)
        elif looking_for == OPERATOR:
            if t.val == ')':
                stack.append(t)
                continue
            assert t.is_op()

            first_operand = stack.pop()
            stack.append(t)
            stack.append(first_operand)
            looking_for = SECOND_OPERAND
        elif looking_for == SECOND_OPERAND:
            if t.is_num():
                stack.append(t)
                looking_for = OPERATOR
            else:
                assert t.val == '('
                stack.append(t)
                looking_for = FIRST_OPERAND
    reduce_stack(stack)
    if len(stack) > 1:
        print stack
    # assert len(stack) == 1
    return stack[-1].val


print calculate(raw_input())
