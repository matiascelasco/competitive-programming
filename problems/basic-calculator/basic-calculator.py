
def operate(o1, op, o2):
    assert op in '+-'
    if op == '+':
        return o1 + o2
    if op == '-':
        return o1 - o2


def is_op(op):
    return op in '+-'


def is_digit(c):
    return ord('0') <= ord(c) <= ord('9')


def pos_value_and_level(s):
    level = 0
    for i, c in enumerate(s):
        if c == ')':
            level -= 1
        yield i, c, level
        if c == '(':
            level += 1


def remove_useless_par(s):
    assert ' ' not in s
    levels = [level for pos, value, level in pos_value_and_level(s)]

    count = 0
    while (s[count] == '(' and ')' == s[-count - 1] and levels[count] == levels[-count - 1] and levels[count] not in levels[count+1:-count-1]):
        count += 1
    return s[count:-count] if count > 0 else s


def find_first_free_op(s):
    level = 0
    for i, c, level in pos_value_and_level(s):
        if level == 0 and is_op(c):
            return i
    raise Exception('Free operator not found in %s' % s)


def calc(s):
    s = remove_useless_par(s)
    if all(is_digit(c) for c in s):
        return int(s)
    op_pos = find_first_free_op(s)
    op = s[op_pos]
    o1 = calc(s[:op_pos])
    o2 = calc(s[op_pos + 1:])
    return operate(o1, op, o2)


def calculate(s):
    s = ''.join(([c for c in s if c != ' ']))
    return calc(s)


print calculate(raw_input())
