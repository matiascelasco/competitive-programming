import Control.Monad (replicateM)


startswith :: String -> String -> Bool
startswith prefix word = take (length prefix) word == prefix

solve_test :: String -> String
solve_test song = if startswith song_as_digits pi then "It's a pi song." else "It's not a pi song."
    where song_as_digits = concat $ map show $ map length $ words $ song
          pi = "3141592653589793238462643383"

main :: IO () 
main = do
    t <- readLn :: IO Int
    songs <- replicateM t getLine
    let 
        ans = map solve_test songs
    mapM_ putStrLn ans
