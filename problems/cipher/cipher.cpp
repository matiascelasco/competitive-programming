#include <iostream>
using namespace std;

int n, k;
char c;
int a[2000000];
int ans[1000000];

int main(){


    cin >> n >> k;
    for (int i = 0; i < n + k; i++){
        cin >> c;
        a[i] = c == '0' ? 0 : 1;
    }
    int accum = 0;
    for (int i = n - 1; i >= 0; i--){
        ans[i] = a[i + k - 1] ^ accum;
        accum ^= ans[i];
        if (i + k - 1 < n){
            accum ^= ans[i + k - 1];
        }
    }
    for (int i = 0; i < n; i++){
        cout << ans[i];
    }
    cout << endl;


    return 0;
}