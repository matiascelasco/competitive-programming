def comb(c, t):
    if t == 0:
        yield []; return
    if len(c) == 0 or t < c[0]:
        return
    x = c[-1]
    for co in comb(c, t - x):
        co.append(x)
        yield co
    c.pop()
    for co in comb(c, t):
        yield co
    c.append(x)

class Solution(object):
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if len(candidates) == 0:
            return []
        candidates.sort()
        return list(comb(candidates, target))
