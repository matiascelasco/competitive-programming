#include <iostream>
using namespace std;

typedef unsigned int uint;

uint a[100000];

uint calc(uint a[], int size){
    if (size % 2 == 0){
        return 0;
    }
    uint ans = 0;
    for (int i = 0; i < size; i += 2){
        ans ^= a[i];
    }
    return ans;
}

int main(){
    uint t, n;
    cin >> t;
    while (t--){
        cin >> n;
        for (uint i = 0; i < n; i++){
            cin >> a[i];
        }
        cout << calc(a, n) << endl;
    }
}