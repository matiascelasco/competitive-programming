#include <iostream>

using namespace std;

typedef long long int lint;

int t, n, a[100001];
lint sum[100001];

int main(){



    int nt;
    cin >> nt;

    for (int t = 1; t <= nt; t++){
        cin >> n;
        for (int i = 1; i <= n; i++){
            cin >> a[i];
        }

        sum[0] = 0;
        lint maxi = a[1];
        for (int i = 1; i <= n; ++i){
            sum[i] = sum[i - 1] + a[i];
            maxi = max(maxi, (lint) a[i]);
        }

        if (maxi <= 0){
            cout << maxi << " " << maxi << endl;
            continue;
        }

        lint ans = 0;
        lint super_min = sum[0];
        for (int i = 1; i <= n; i++){
            super_min = min(super_min, sum[i]);
            ans = max(ans, sum[i] - super_min);
        }
        cout << ans << " ";

        ans = 0;
        for (int i = 1; i <= n; i++){
            ans += a[i] * (a[i] > 0);
        }
        cout << ans << endl;
    }

    return 0;
}