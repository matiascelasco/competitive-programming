module LonelyInteger where

import Control.Monad (replicateM, mapM_)
import Data.Char (ord)


line_values :: String -> [Int]
line_values = (map read) . words

quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = first_half ++ [x] ++ second_half
    where first_half = quicksort $ filter (<= x) xs
          second_half = quicksort $ filter (> x) xs

solve_test :: [Int] -> Int
solve_test = rec Nothing . quicksort
    where rec (Just prev) [] = prev
          rec Nothing (x:xs) = rec (Just x) xs
          rec (Just prev) (x:xs) | x == prev = rec Nothing xs
                                 | otherwise = prev

main :: IO ()
main = do
    n <- readLn :: IO Int
    line <- getLine :: IO String
    let
        tests = map line_values [line]
        ans = map solve_test tests
    mapM_ print ans
