from itertools import chain
from string import digits


def get_rows(board):
    return board


def get_cols(board):
    width = len(board[0])
    return [[row[i] for row in board] for i in xrange(width)]


def get_boxes(board):
    strips_of_rows = (board[i*3:i*3+3] for i in xrange(3))
    strips_of_cols = (get_cols(strip) for strip in strips_of_rows)
    return [list(chain(*strip[i*3:i*3+3])) for i in xrange(3) for strip in strips_of_cols]


def is_valid(values):
    valid_chars = digits + '.'
    if not all(value in valid_chars for value in values):
        return False
    excluding_spaces = [value for value in values if value != '.']
    return len(excluding_spaces) == len(set(excluding_spaces))


# @param {character[][]} board
# @return {boolean}
def is_valid_sudoku(board):
    return (all(is_valid(row) for row in get_rows(board)) and
            all(is_valid(col) for col in get_cols(board)) and
            all(is_valid(box) for box in get_boxes(board)))


board = [raw_input().split(' ') for _ in xrange(9)]

print 'YES' if is_valid_sudoku(board) else 'NO'
