def is_valid(board, x, y, w, h):
    counter = [0 for _ in xrange(10)]
    for i in xrange(x, x + h):
        for j in xrange(y, y + w):
            d = board[i][j]
            if d != '.':
                try:
                    d = int(d)
                except ValueError:
                    return False
                try:
                    if counter[d] == 1:
                        return False
                    counter[d] += 1
                except IndexError:
                    return False
    return True


# @param {character[][]} board
# @return {boolean}
def is_valid_sudoku(board):
    for i in xrange(9):
        if not is_valid(board, i, 0, 9, 1):
            return False

    for i in xrange(9):
        if not is_valid(board, 0, i, 1, 9):
            return False

    for i in xrange(9):
        if not is_valid(board, (i / 3) * 3, (i % 3) * 3, 3, 3):
            return False

    return True


board = [raw_input().split(' ') for _ in xrange(9)]

print 'YES' if is_valid_sudoku(board) else 'NO'
