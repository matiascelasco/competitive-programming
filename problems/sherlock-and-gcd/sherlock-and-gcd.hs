import Control.Monad (replicateM, mapM_)


cartSquare :: [Int] -> [(Int, Int)]
cartSquare set = [(x, y) | x <- set, y <- set]

readTestCase :: IO [Int]
readTestCase = do
    _ <- getLine
    line <- getLine
    return $ map read (words line)

solveTest :: [Int] -> String
solveTest xs = if satisfiesCondition then "YES" else "NO"
    where
    satisfiesCondition = any (\(x, y) -> gcd x y == 1) (cartSquare xs)

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTest testCases
    mapM_ putStrLn ans