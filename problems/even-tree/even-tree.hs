import Control.Monad (replicateM)
import Data.Vector (fromList, (!))
import Data.Maybe (fromJust)

data Tree t = Node [Tree t]


--utils
boolAsPair :: Bool -> (Int, Int)
boolAsPair True = (1, 0)
boolAsPair False = (0, 1)

pairAddition :: (Int, Int) -> (Int, Int) -> (Int, Int)
pairAddition (a, b) (c, d) = (a + c, b + d)

pairSum :: [(Int, Int)] -> (Int, Int)
pairSum = foldr pairAddition (0, 0)

splittedCount :: (t -> Bool) -> [t] -> (Int, Int)
splittedCount f xs = pairSum (map (boolAsPair . f) xs)

getTheOtherFromPair :: Int -> (Int, Int) -> Int
getTheOtherFromPair target (x, y)
    | target == x = y
    | target == y = x
    | otherwise = error ((show target) ++ " not included in pair " ++ (show (x, y)))

pairIncludes :: Int -> (Int, Int) -> Bool
pairIncludes target (x, y)
    | target == x = True
    | target == y = True
    | otherwise = False


--solution
solve :: Tree Int -> Int
solve tree = removedEdgesCount
    where
    (removedEdgesCount, _) = solveSubTree tree
    solveSubTree (Node []) = (0, False)
    solveSubTree (Node children) = (totalEdgesRemoved, isEven)
        where
        totalEdgesRemoved = evenChildrenCount + (sum edgesRemovedByChildrenCount)
        isEven = odd oddChildrenCount
        (evenChildrenCount, oddChildrenCount) = splittedCount id areChildrenEven
        (edgesRemovedByChildrenCount, areChildrenEven) = unzip $ map solveSubTree children


--input parsing
buildTree :: Int -> [(Int, Int)] -> (Tree Int)
buildTree n edges = build Nothing 0
    where
    build maybeParentIndex rootIndex = Node (map (build (Just rootIndex)) childrenIndexes)
        where
        childrenIndexes
            | maybeParentIndex == Nothing = adj
            | otherwise =  filter (/= (fromJust maybeParentIndex)) adj
            where
            adj = vectorOfListsOfAdj ! rootIndex
        vectorOfListsOfAdj = fromList listOfListsOfAdj
        listOfListsOfAdj = map buildChildrenList [0..(n - 1)]
        buildChildrenList parent = map (getTheOtherFromPair parent) parentEdges
            where
            parentEdges = filter (pairIncludes parent) zeroIndexedEdges
        zeroIndexedEdges = map (\(a, b) -> (a - 1, b - 1)) edges


readTree :: IO (Tree Int)
readTree = do
    (n, m) <- readPairOfInts
    edges <- replicateM m readPairOfInts
    let
        tree = buildTree n edges
    return tree

readPairOfInts :: IO (Int, Int)
readPairOfInts = do
    line <- getLine
    let
        [a, b] = map read (words line)
    return (a, b)

main :: IO ()
main = do
    tree <- readTree
    let
        ans = solve tree
    print ans
