import Control.Monad (replicateM, mapM_)
import Data.Vector (fromList, (!), length)

solve :: [Int] -> Int -> [Int] -> [Int]
solve nums k qs = map (vector !) positions
    where
    positions = map (shift k) qs
    shift offset x = (x - offset) `mod` len
    len = Data.Vector.length vector
    vector = fromList nums


readThreeNumbers :: IO (Int, Int, Int)
readThreeNumbers = do
    line <- getLine
    let
        [a, b, c] = map read (words line)
    return (a, b, c)

readIntList :: IO [Int]
readIntList = do
    line <- getLine
    return (map read $ words line)

main :: IO ()
main = do
    (_, k, q) <- readThreeNumbers
    nums <- readIntList
    qs <- replicateM q readLn
    let
        ans = solve nums k qs
    mapM_ print ans