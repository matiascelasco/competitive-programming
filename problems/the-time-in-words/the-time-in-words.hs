import Control.Monad ()

numberAsWord :: Int -> String
numberAsWord 0 = "zero"
numberAsWord 1 = "one"
numberAsWord 2 = "two"
numberAsWord 3 = "three"
numberAsWord 4 = "four"
numberAsWord 5 = "five"
numberAsWord 6 = "six"
numberAsWord 7 = "seven"
numberAsWord 8 = "eight"
numberAsWord 9 = "nine"
numberAsWord 10 = "ten"
numberAsWord 11 = "eleven"
numberAsWord 12 = "twelve"
numberAsWord 13 = "thirteen"
numberAsWord 14 = "fourteen"
numberAsWord 15 = "fifteen"
numberAsWord n | 16 <= n && n <= 19 = numberAsWord (n `mod` 10) ++ "teen"
numberAsWord 20 = "twenty"
numberAsWord 30 = "thirty"
numberAsWord 40 = "fourty"
numberAsWord 50 = "fifty"
numberAsWord 60 = "sixty"
numberAsWord n = numberAsWord ((n `div` 10) * 10) ++ " " ++ numberAsWord (n `mod` 10)


timeInWords :: (Int, Int) -> String
timeInWords (h, m) | m == 0 = hWord ++ " o' clock"
                   | m == 1 = mWord ++ " minute past " ++ hWord
                   | m == 15 = "quarter past " ++ hWord
                   | m < 30 = mWord ++ " minutes past " ++ hWord
                   | m == 30 = "half past " ++ hWord
                   | m == 45 = "quarter to " ++ hNextWord
                   | m > 30 = numberAsWord (60 - m) ++ " minutes to " ++ hNextWord
                   | otherwise = error "invalid hour"
                   where
                   hNextWord = hPlusWord 1
                   hPlusWord x = numberAsWord $ ((h + x - 1) `mod` 12) + 1
                   mWord = numberAsWord m
                   hWord = numberAsWord h

main :: IO ()
main = do
    h <- readLn
    m <- readLn
    let
        ans = timeInWords (h, m)
    putStrLn ans