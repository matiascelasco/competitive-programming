#include <iostream>
#include <vector>
#include <stack>
#include <set>
using namespace std;

int n, m, k, q, u, v, b_i;
vector<int> adj[100000];
vector<int> rev[100000];
int grundy[100000];
int unvisited_followers[100000];

int main(){

    cin >> n;
    for (int i = 0; i < n; i++){
        unvisited_followers[i] = 0;
    }
    cin >> m;
    for (int i = 0; i < m; i++){
        cin >> u;
        cin >> v;
        u--;
        v--;
        adj[u].push_back(v);
        rev[v].push_back(u);
        unvisited_followers[u]++;
    }

    //calculate_grundy_numbers
    stack<int> s;
    for (int i = 0; i < n; i++){
        if (unvisited_followers[i] == 0){
            s.push(i);
        }
    }
    while (!s.empty()){
        int current = s.top();
        s.pop();
        set<int> followers_grundys;
        for (unsigned int i = 0; i < adj[current].size(); i++){
            followers_grundys.insert(grundy[adj[current][i]]);
        }
        grundy[current] = 0;
        while (followers_grundys.count(grundy[current])){
            grundy[current]++;
        }

        for (unsigned int i = 0; i < rev[current].size(); i++){
            unvisited_followers[rev[current][i]]--;
            if (unvisited_followers[rev[current][i]] == 0){
                s.push(rev[current][i]);
            }
        }
    }

    cin >> q;
    while(q--){
        cin >> k;
        int total_xor = 0;
        for (int i = 0; i < k; i++){
            cin >> b_i;
            b_i--;
            total_xor = total_xor xor grundy[b_i];
        }
        cout << (total_xor ? "Bumi": "Iroh") << endl;
    }

    return 0;
}