import Control.Monad (replicateM, mapM_)

solveTestCase :: (Integer, Integer, Integer, Integer, Integer) -> Integer
solveTestCase (b, w, x, y, z) = minimum [b * x + w * y, b * (y + z) + w * y, b * x + w * (x + z)]

readTestCase :: IO (Integer, Integer, Integer, Integer, Integer)
readTestCase = do
    firstLine <- getLine
    secondLine <- getLine
    let
        [b, w] = map read $ words firstLine
        [x, y, z] = map read $ words secondLine
    return (b, w, x, y, z)

main :: IO ()
main = do
    t <- readLn
    testCases <- replicateM t readTestCase
    let
        ans = map solveTestCase testCases
    mapM_ print ans