#include <iostream>
#include <string>
#include <cctype>
using namespace std;

typedef unsigned int uint;

struct Node {
    int val;
    Node *prev;
    Node *next;
    Node *child;
};

// [[1, 2, 3]]

inline bool is_digit(const char &c){
    return '0' <= c and c <= '9';
}

int parse(const string &s, uint &pos){
    int x = 0;
    while (is_digit(s[pos])){
        x *= 10;
        x += s[pos] - '0';
        ++pos;
    }
    return x;
}

// [[1, 2, 3]]
// 012345678

uint match(const string &s, uint pos){
    int count = 1;
    pos++;
    while (pos < s.size() and count > 0){
        if (s[pos] == '[') ++count;
        if (s[pos] == ']') --count;
        ++pos;
    }
    return pos - 1;
}

Node* build_list(const string &s, uint lo, uint hi){

    if (lo >= hi){
        return NULL;
    }

    uint close;
    Node* n;
    if (s[lo] == '['){
        close = match(s, lo);
        n = build_list(s, lo + 1, close);
        n->child = n->next;
        n->next = NULL;
        lo = close + 1;
    }
    else {
        n = new Node();
        n->val = parse(s, lo);
        n->prev = NULL;
        n->next = NULL;
        n->child = NULL;
    }

    if (s[lo] == ','){
        n->next = build_list(s, lo + 1, hi);
        if (n->next != NULL){
            n->next->prev = n;
        }
    }

    return n;
}

void print_flat(Node *n){
    if (n == NULL){
        return;
    }
    while (n != NULL){
        cout << (n->val) << " ";
        n = n->next;
    }
    cout << endl;
}

Node* flat(Node *n){
    Node* tail = NULL;
    while (n != NULL){
        Node* old_next = n->next;
        if (n->child != NULL){
            tail = flat(n->child);
            n->next = n->child;
            n->child->prev = n;
            tail->next = old_next;
            if (old_next != NULL){
                old_next->prev = tail;
            }
        }
        else {
            tail = n;
        }
        n = old_next;
    }

    return tail;
}

void print_tree(Node *n, int level){
    if (n == NULL){
        return;
    }
    for (int i = 0; i < level; i++){
        cout << "    ";
    }
    cout << (n->val) << endl;
    print_tree(n->child, level + 1);
    print_tree(n->next, level);
}

void print_tree(Node *n){
    print_tree(n, 0);
}

int main(){
    string s;
    getline(cin, s);
    Node *n = build_list(s, 1, s.size() - 1);
    print_tree(n);
    flat(n);
    cout << "#################################################" << endl;
    print_flat(n);
}