class Node(object):

    def __init__(self):
        self.value = None
        self.prev = None
        self.next = None
        self.child = None


def parse(s, pos):
    x = 0
    while (s[pos] in '0123456789'):
        x *= 10
        x += int(s[pos])
        pos += 1
    return x, pos


def match(x, pos):
    count = 1
    pos += 1
    while pos < len(s) and count > 0:
        if s[pos] == '[':
            count += 1
        if s[pos] == ']':
            count -= 1
        pos += 1
    return pos - 1


def build_list(s, lo, hi):

    if lo >= hi:
        return None

    if s[lo] == '[':
        close = match(s, lo)
        n = build_list(s, lo + 1, close)
        n.child = n.next
        n.next = None
        lo = close + 1
    else:
        n = Node()
        n.value, lo = parse(s, lo)

    if s[lo] == ',':
        n.next = build_list(s, lo + 1, hi)
        if n.next is not None:
            n.next.prev = n

    return n


def print_flat(n):
    if n is None:
        return
    while n is not None:
        print n.value,
        n = n.next
    print


def print_tree(n, level=0):

    if n is None:
        return

    for i in range(level):
        print "   ",
    print n.value
    print_tree(n.child, level + 1)
    print_tree(n.next, level)


def flat(n):
    tail = None
    while n is not None:
        old_next = n.next
        if n.child is not None:
            tail = flat(n.child)
            n.next = n.child
            n.child.prev = n
            tail.next = old_next
            if old_next is not None:
                old_next.prev = tail
        else:
            tail = n
        n = old_next
    return tail

s = raw_input()
n = build_list(s, 1, len(s) - 1)
print_tree(n)
flat(n)
print "#################################################"
print_flat(n)
