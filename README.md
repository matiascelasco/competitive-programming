I will use this repo to keep my solutions to competitive programming problems from sites such as Hackerrank, Leetcode, Codeforces, SPOJ, etc.

It also includes a helpul bash script that automate some boilerplate tasks like setting up a new problem directory, define test cases, run the code against those cases and tag the problems.