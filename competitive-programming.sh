if [[ ! $- = *i* ]]; then
    echo "Error: this script must be called using source"
    exit 1
fi

# path of the directory which contains this script
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)


command=$1

function hr {
    echo "--------------------------------------------------------------------------------"
}

function cat2 {
    # this awesome version of cat prints and extra line break if the file doesn't finish with it
    # it also prints just the head if the file is too long
    last_byte=$(tail -c 1 $1)
    size=$(stat --printf="%s" $1)
    if [[ $size -lt 1000 ]]; then
        cat $1
        if [ "$last_byte" != "" ]; then
            echo
        fi
    else
        head --bytes=100 $1
        echo "... TOO LONG"
    fi
}

function test_case_result {
    test_case=$1
    result=$2
    input_file=$3
    output_file=$4
    expected_output_file=$5
    errors_file=$6
    echo "Test case $test_case --> $result"
    if [ "$result" == "ERROR" ]; then
        echo
        cat2 "$errors_file"
        echo
    elif [ "$result" != "OK" ]; then
        echo
        echo "input:"
        cat2 "$input_file"
        echo
        echo "output:"
        cat2 "$output_file"
        echo
        echo
        if [ "$result" == "WRONG" ]; then
            echo "expected output:"
            cat2 "$expected_output_file"
        else
            echo "expected output not provided"
        fi
        echo
    fi
}

function run_file {
    if [ "$ugly_flag" = false ]; then
        read
    fi
    ugly_flag=false
    filename=$1
    extension="${filename##*.}"
    case $extension in
        "hs")
            runable="${filename%.*}"
            if [ -e "$runable.o" ]; then
                rm "$runable.o"
            fi
            # ghc -O -Wall +RTS -M10m -RTS -fno-warn-type-defaults $filename 2> "compilation-errors.txt" > /dev/null
            ghc -O -Wall +RTS -M100m -RTS $filename 2> "compilation-errors.txt" > /dev/null
            if [ -s "compilation-errors.txt" ]; then
                hr
                echo "COMPILATION ERROR: $filename"
                hr
                cat2 "compilation-errors.txt"
                echo
                return 1;
            else
                if [ -e "compilation-errors.txt" ]; then
                    rm "compilation-errors.txt"
                fi
            fi
            hr
            echo "Testing $filename"
            hr
            for test_case in $(/bin/ls "test-cases"); do
                test_case_dir="test-cases/$test_case"
                input_file="$test_case_dir/input.txt"
                output_file="$test_case_dir/output.txt"
                errors_file="$test_case_dir/errors.txt"
                expected_output_file="$test_case_dir/expected-output.txt"
                time ./"$runable" < $input_file > $output_file 2> $errors_file
                if [ -s "$errors_file" ]; then
                    result="ERROR"
                elif [ -e "$expected_output_file" ]; then
                    difference=$(diff --ignore-all-space $output_file $expected_output_file)
                    if [ -z "$difference" ]; then
                        result="OK"
                    else
                        result="WRONG"
                    fi
                else
                    result="Unknown"
                fi
                test_case_result $test_case $result $input_file $output_file $expected_output_file $errors_file
            done
            ;;
        "py")
            hr
            echo "Testing $filename"
            hr
            for test_case in $(/bin/ls "test-cases"); do
                test_case_dir="test-cases/$test_case"
                input_file="$test_case_dir/input.txt"
                output_file="$test_case_dir/output.txt"
                errors_file="$test_case_dir/errors.txt"
                expected_output_file="$test_case_dir/expected-output.txt"
                time python "$filename" < $input_file > $output_file 2> $errors_file
                if [ -s "$errors_file" ]; then
                    result="ERROR"
                elif [ -e "$expected_output_file" ]; then
                    difference=$(diff --ignore-all-space $output_file $expected_output_file)
                    if [ -z "$difference" ]; then
                        result="OK"
                    else
                        result="WRONG"
                    fi
                else
                    result="Unknown"
                fi
                test_case_result $test_case $result $input_file $output_file $expected_output_file $errors_file
            done
            ;;
        "cpp")
            runable="${filename%.*}"
            if [ -e "$runable.out" ]; then
                rm "$runable.out"
            fi
            g++ -o "$runable.out" -Wall $filename 2> "compilation-errors.txt" > /dev/null
            if [ -s "compilation-errors.txt" ]; then
                hr
                echo "COMPILATION ERROR: $filename"
                hr
                cat2 "compilation-errors.txt"
                echo
                return 1;
            else
                if [ -e "compilation-errors.txt" ]; then
                    rm "compilation-errors.txt"
                fi
            fi
            hr
            echo "Testing $filename"
            hr
            for test_case in $(/bin/ls "test-cases"); do
                test_case_dir="test-cases/$test_case"
                input_file="$test_case_dir/input.txt"
                output_file="$test_case_dir/output.txt"
                errors_file="$test_case_dir/errors.txt"
                expected_output_file="$test_case_dir/expected-output.txt"
                time ./"$runable.out" < $input_file > $output_file 2> $errors_file
                if [ -s "$errors_file" ]; then
                    result="ERROR"
                elif [ -e "$expected_output_file" ]; then
                    difference=$(diff --ignore-all-space $output_file $expected_output_file)
                    if [ -z "$difference" ]; then
                        result="OK"
                    else
                        result="WRONG"
                    fi
                else
                    result="Unknown"
                fi
                test_case_result $test_case $result $input_file $output_file $expected_output_file $errors_file
            done
    esac
    echo
}

case $command in
    "create_problem" )
        if [ "$#" -lt 3 ]; then
            echo "Usage: create_problem problem_name ext [*tag]"
            return 1;
        fi
        problem_name=$2
        extension=$3
        problem_dir="$DIR/problems/$problem_name"
        mkdir $problem_dir
        mkdir "$problem_dir/test-cases"
        shift 2
        for tag in "$@"; do
            mkdir "$DIR/tags/$tag/"
            ln -s "$problem_dir" "$DIR/tags/$tag/$problem_name"
        done
        ln -s "$problem_dir" "$DIR/tags/unsolved/$problem_name"
        cd $problem_dir
        # TODO: make this generic, allowing other text editors
        subl "$problem_dir/$problem_name.$extension"
        ;;
    "create_test_case" )
        if [ ! -d "test-cases" ]; then
            # echo "You are in the wrong directory"
            echo "You are in the wrong neighborhood"
            return 1;
        fi
        case_name=$2
        if [ -z "$case_name" ]; then
            echo "You need to specify the name of the test case"
            return 1;
        fi
        mkdir "test-cases/$case_name"
        subl "test-cases/$case_name/input.txt" "test-cases/$case_name/expected-output.txt"
        ;;
    "accept_output" )
        if [ ! -d "test-cases" ]; then
            # echo "You are in the wrong directory"
            echo "You are in the wrong neighborhood"
            return 1;
        fi
        case_name=$2
        if [ -z "$case_name" ]; then
            echo "You need to specify the name of the test case"
            return 1;
        fi
        mkdir "test-cases/$case_name"
        cp "test-cases/$case_name/output.txt" "test-cases/$case_name/expected-output.txt"
        ;;
    "run")
        if [ ! -d "test-cases" ]; then
            echo "You are in the wrong directory";
            return 1;
        fi
        problem_dir=$(pwd)
        source_file=$2
        ugly_flag=true
        if [ -n "$source_file" ]; then
            run_file $source_file;
        else
            for source_file in $(/bin/ls | grep .hs$); do
                run_file $source_file;
            done
            for source_file in $(/bin/ls | grep .py$); do
                run_file $source_file;
            done
            for source_file in $(/bin/ls | grep .cpp$); do
                run_file $source_file;
            done
        fi
        ;;
    "solved")
        problem_name=$2
        if [ -z "$problem_name" ]; then
            problem_name=$(basename $(pwd))
        fi
        problem_dir="$DIR/problems/$problem_name"
        ln -s "$problem_dir" "$DIR/tags/solved/$problem_name"
        rm "$DIR/tags/unsolved/$problem_name"
        ;;
    "tag")
        tag=$2
        problem_name=$3
        if [ -z "$problem_name" ]; then
            problem_name=$(basename $(pwd))
        fi
        problem_dir="$DIR/problems/$problem_name"
        if [ ! -e "$DIR/tags/$tag" ]; then
            mkdir "$DIR/tags/$tag"
        fi
        ln -s "$problem_dir" "$DIR/tags/$tag/$problem_name"
        ;;
    "remove_tag")
        tag=$2
        problem_name=$3
        if [ -z "$problem_name" ]; then
            problem_name=basename $(pwd)
        fi
        rm "$DIR/tags/$tag/$problem_name"
        ;;
    "tree")
        tree "$DIR"
        ;;
    "tags_tree")
        tree "$DIR/tags"
        ;;
    "stats")
        echo "problems $(/bin/ls $DIR/problems | wc -l)"
        echo
        for tag in $(/bin/ls "$DIR/tags/"); do
            echo "$tag $(/bin/ls $DIR/tags/$tag | wc -l)"
        done
        ;;
    "-h" | "--help" | "help")
        echo "create_problem problem_name ext [*tag] |"
        echo "create_test_case case_name |"
        echo "accept_output case_name |"
        echo "run [filename] |"
        echo "solved [problem_name] |"
        echo "tag tag_name [problem_name] |"
        echo "remove_tag tag_name [problem_name] |"
        echo "tree |"
        echo "tags_tree |"
        echo "stats"
        ;;
    *)
        # TODO: do this in every command using a flag in order to dont include in more than once
        PATH=$PATH:$DIR
        cd $DIR
esac