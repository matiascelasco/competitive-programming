module AvlTree (empty, insert, fromList, height, size) where
import Data.List (intercalate)

data AvlTree t = EmptyAvlTree | AvlTree { value :: t
                            , count :: Int
                            , _size :: Int
                            , _height :: Int
                            , left :: AvlTree t
                            , right :: AvlTree t
                            }

height :: (AvlTree t) -> Int
height EmptyAvlTree = 0
height t = _height t

size :: (AvlTree t) -> Int
size EmptyAvlTree = 0
size t = _size t


balanceFactor :: (AvlTree t) -> Int
balanceFactor EmptyAvlTree = 0
balanceFactor t | _height t == 1 = 0
                | otherwise = (height $ left $ t) - (height $ right $ t)

rotateLeft :: (AvlTree t) -> (AvlTree t)
rotateLeft t = (left t) {
        _size = size t,
        _height = 1 + max (height l) (height r),
        left = l,
        right = r
    }
    where
    l = t1
    r = t {
        _size = sum [size t2, size t3, count t],
        _height = 1 + max (height t2) (height t3),
        left = t2, right = t3
    }
    t1 = left (left t)
    t2 = right (left t)
    t3 = right t

rotateRight :: (AvlTree t) -> (AvlTree t)
rotateRight t = (right t) {
        _size = size t,
        _height = 1 + max (height l) (height r),
        left = l, right = r
    }
    where
    l = t {
        _size = sum [size t2, size t3, count t],
        _height = 1 + max (height t2) (height t3),
        left = t3, right = t2
    }
    r = t1
    t1 = right (right t)
    t2 = left (right t)
    t3 = left t

rotateDoubleLeft :: (AvlTree t) -> (AvlTree t)
rotateDoubleLeft t = rotateLeft $ t {left = rotateRight (left t)}

rotateDoubleRight :: (AvlTree t) -> (AvlTree t)
rotateDoubleRight t = rotateRight $ t {right = rotateLeft (right t)}

insert :: (Ord t) => (AvlTree t) -> t -> (AvlTree t)
insert EmptyAvlTree val = AvlTree {value=val, count=1, _size=1, _height=1, left=EmptyAvlTree, right=EmptyAvlTree}
insert t x
    | isLL = rotateLeft updatedNode
    | isRR = rotateRight updatedNode
    | isRL = rotateDoubleRight updatedNode
    | isLR = rotateDoubleLeft updatedNode
    | otherwise = updatedNode
    where
        isLL = f > 1 && lf > 0
        isLR = f > 1 && lf < 0
        isRL = f < (-1) && rf > 0
        isRR = f < (-1) && rf < 0
        f = balanceFactor updatedNode
        lf = balanceFactor $ left updatedNode
        rf = balanceFactor $ right updatedNode
        updatedNode
            | x > value t = t {
                _size = size t + 1,
                _height =  1 + max (height (left t)) (height insertedAtRight),
                right = insertedAtRight
            }
            | x < value t = t {
                _size = size t + 1,
                _height = 1 + max (height insertedAtLeft) (height (right t)),
                left = insertedAtLeft
            }
            | otherwise = t {
                count = count t + 1,
                _size = size t + 1
            }
           where
           insertedAtLeft = insert (left t) x
           insertedAtRight = insert (right t) x

empty :: (AvlTree t)
empty = EmptyAvlTree

fromList :: (Ord t) => [t] -> (AvlTree t)
fromList = foldl insert EmptyAvlTree

instance (Show t) => Show (AvlTree t) where
    show EmptyAvlTree = "*"
    show t = intercalate " " [
            showChild (left t),
            (if count t > 1 then (show $ count t) ++ "*" else "") ++ "[" ++ show (value t) ++ "]",
            "h=" ++ show (height t),
            "s=" ++ show (size t),
            showChild (right t)
        ]
        where
        showChild child = "(" ++ (show child) ++ ")"

showByLevels ::(Show t) => (AvlTree t) -> String
showByLevels EmptyAvlTree = "*"
showByLevels t = concat $ map showLevel [0.._height t - 1]
    where
    showLevel level = showLevel' 0 (show t)
        where
        showLevel' _ "" = ['\n']
        showLevel' currentLevel (x:xs) = newX:(showLevel' newCurrentLevel xs)
            where
            (newCurrentLevel, newX)
                | x == '(' = (currentLevel + 1, ' ')
                | x == ')' = (currentLevel - 1, ' ')
                | otherwise = (currentLevel, if (currentLevel == level) then x else ' ')


compareAndCount :: (Ord t) => (AvlTree t) -> t -> (Int, Int, Int)
compareAndCount EmptyAvlTree target = (0, 0, 0)
compareAndCount t@(AvlTree {}) target =
    target < value t = (leftLt, leftEq, leftGt + count t + size (right t))
    target == value t = (size (left t), count t, size (right t))
    value t < target = (count t + size (left t) + rightLt, rightEq, rightGt)
    where
    (leftLt, leftEq, leftGt) = compareAndCount (left t) target
    (rightLt, rightEq, rightGt) = compareAndCount (right t) target



getFirst :: (t, t, t) -> t
getFirst (x, _, _) = x

getSecond :: (t, t, t) -> t
getSecond (_, x, _) = x

getThird :: (t, t, t) -> t
getThird (_, _, x) = x


countLt :: (Ord t) => (AvlTree t) -> t -> Int
countLt = getFirst . compareAndCount

countLte :: (Ord t) => (AvlTree t) -> t -> Int
countLte t target = size t - countGt t

countEq :: (Ord t) => (AvlTree t) -> t -> Int
countEq = getSecond . compareAndCount

countGte :: (Ord t) => (AvlTree t) -> t -> Int
countGte t target =size t - countLt t

countGt :: (Ord t) => (AvlTree t) -> t -> Int
countGt = getThird . compareAndCount