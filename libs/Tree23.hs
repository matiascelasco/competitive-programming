module Tree23 (empty, insert, fromList, countLt, countLte, countEq, countGte, countGt, size) where
import Data.List (sort)
import Text.Printf (printf)
import Data.List (intercalate)

data Tree23 t =
    EmptyTree23 |
    TreeNode2 {
        value :: t,
        count :: Int,
        _size :: Int,
        left :: (Tree23 t),
        right :: (Tree23 t)
    } |
    TreeNode3 {
        valueLeft :: t,
        valueRight :: t,
        countLeft :: Int,
        countRight :: Int,
        _size :: Int,
        left :: (Tree23 t),
        center :: (Tree23 t),
        right :: (Tree23 t)
    }



isLeaf :: (Tree23 t) -> Bool
isLeaf EmptyTree23 = False
isLeaf TreeNode2 {left = EmptyTree23, right = EmptyTree23} = True
isLeaf TreeNode3 {left = EmptyTree23, right = EmptyTree23} = True
isLeaf _ = False

insert :: (Ord t) => (Tree23 t) -> t -> (Tree23 t)
insert EmptyTree23 value = TreeNode2 {
    value = value,
    count = 1,
    left = EmptyTree23,
    right = EmptyTree23,
    _size = 1
}
insert t@(TreeNode2 {}) newValue
    | newValue < value t =
        let
        (newLeft, splitted) = insertAndMaybeSplit (left t) newValue in
        if isLeaf t then TreeNode3 {
            valueLeft = newValue,
            valueRight = value t,
            countLeft = 1,
            countRight = count t,
            left = EmptyTree23,
            center = EmptyTree23,
            right = EmptyTree23,
            _size = size t + 1
        }
        else if not splitted then t {
            left = newLeft,
            _size = size t + 1
        }
        else TreeNode3 {
            left = left newLeft,
            valueLeft = value newLeft,
            countLeft = count newLeft,
            center = right newLeft,
            valueRight = value t,
            countRight = count t,
            right = right t,
            _size = size t + 1
        }
    | newValue > value t =
        let
        (newRight, splitted) = insertAndMaybeSplit (right t) newValue in
        if isLeaf t then TreeNode3 {
            valueLeft = value t,
            valueRight = newValue,
            countLeft = count t,
            countRight = 1,
            left = EmptyTree23,
            center = EmptyTree23,
            right = EmptyTree23,
            _size = size t + 1
        }
        else if not splitted then t {
            right = newRight,
            _size = size t + 1
        }
        else TreeNode3 {
            left = left t,
            valueLeft = value t,
            countLeft = count t,
            center = left newRight,
            valueRight = value newRight,
            countRight = count newRight,
            right = right newRight,
            _size = size t + 1
        }
    | otherwise = t {  -- newValue == value t
        count = count t + 1,
        _size = size t + 1
    }
insert t@(TreeNode3 {}) newValue = fst $ insertAndMaybeSplit t newValue

createNode :: t -> Int -> (Tree23 t)
createNode value count = TreeNode2 {
        value = value,
        count = count,
        left = EmptyTree23,
        right = EmptyTree23,
        _size = count
    }

insertAndMaybeSplit :: (Ord t) => (Tree23 t) -> t -> (Tree23 t, Bool)
insertAndMaybeSplit EmptyTree23 _ = error "It doesn't make sense to do this on an empty tree"
insertAndMaybeSplit t@(TreeNode2 {}) newValue = (insert t newValue, False)
insertAndMaybeSplit t@(TreeNode3 {}) newValue
    | newValue == valueLeft t = (t {countLeft = countLeft t + 1, _size = size t + 1}, False)
    | newValue == valueRight t = (t {countRight = countRight t + 1, _size = size t + 1}, False)
    | isLeaf t =
        let
        values = [(newValue, 1), (valueLeft t, countLeft t), (valueRight t, countRight t)]
        [(vl, cl), (cv, cc), (vr, cr)] = sort values in
        (TreeNode2 {
            value = cv,
            count = cc,
            left = createNode vl cl,
            right = createNode vr cr,
            _size = cl + cc + cr
        }, True)
    | newValue < valueLeft t =
        let (newLeft, splitted) = insertAndMaybeSplit (left t) newValue in
        if not splitted then (t {
            left = newLeft,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = valueLeft t,
            count = countLeft t,
            left = newLeft,
            right = TreeNode2 {
                value = valueRight t,
                count = countRight t,
                left = center t,
                right = right t,
                _size = size (right t) + size (center t) + countRight t
            },
            _size = size t + 1
        }, True)
    | newValue > valueRight t =
        let (newRigth, splitted) = insertAndMaybeSplit (right t) newValue in
        if not splitted then (t {
            right = newRigth,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = valueRight t,
            count = countRight t,
            left = TreeNode2 {
                value = valueLeft t,
                count = countLeft t,
                left = left t,
                right = center t,
                _size = size (left t) + size (center t) + countLeft t
            },
            right = newRigth,
            _size = size t + 1
        }, True)
    | otherwise = -- center
        let (newCenter, splitted) = insertAndMaybeSplit (center t) newValue in
        if not splitted then (t {
            center = newCenter,
            _size = size t + 1
        }, False)
        else (TreeNode2 {
            value = value newCenter,
            count = count newCenter,
            left = TreeNode2 {
                value = valueLeft t,
                count = countLeft t,
                left = left t,
                right = left newCenter,
                _size = sum (map (size . left) [t, newCenter]) + countLeft t
            },
            right = TreeNode2 {
                value = valueRight t,
                count = countRight t,
                left = right newCenter,
                right = right t,
                _size = sum (map (size . right) [t, newCenter]) + countRight t
            },
            _size = size t + 1
        }, True)


size :: (Tree23 t) -> Int
size EmptyTree23 = 0
size t = _size t

empty :: (Tree23 t)
empty = EmptyTree23

fromList :: (Ord t) => [t] -> (Tree23 t)
fromList = foldl insert EmptyTree23

height :: (Tree23 t) -> Int
height EmptyTree23 = 0
height t = 1 + height (left t)

instance (Show t) => Show (Tree23 t) where
    show EmptyTree23 = "."
    show t@TreeNode2 {left = l, value = v, right = r} =
        printf "(%s){%s*[%s]}(%s)" (show l) (show (count t)) (show v) (show r)
    show t@TreeNode3 {left = l, valueLeft = vl, center = c, valueRight = vr, right = r} =
        printf "(%s){%s*[%s] (%s) %s*[%s]}(%s)" (show l)
                                                (show (countLeft t)) (show vl)
                                                (show c)
                                                (show (countRight t)) (show vr)
                                                (show r)

showByLevels :: (Show t) => (Tree23 t) -> String
showByLevels t = intercalate "\n" (map showLevel [0..height t - 1])
    where
    plain = show t
    showLevel level = showLevel' 0 plain
        where
        showLevel' _ [] = []
        showLevel' curLevel (x:xs)
            | isTheLevel = x:rest
            | otherwise = ' ':rest
            where
            rest = (showLevel' newCurLevel xs)
            (newCurLevel, isTheLevel) | x == '(' = (curLevel + 1, False)
                                      | x == ')' = (curLevel - 1, False)
                                      | otherwise = (curLevel, curLevel == level)


compareAndCount :: (Ord t) => (Tree23 t) -> t -> (Int, Int, Int)
compareAndCount EmptyAvlTree target = (0, 0, 0)
compareAndCount t@(TreeNode2 {}) target =
    target < value t = (leftLt, leftEq, leftGt + count t + size (right t))
    target == value t = (size (left t), count t, size (right t))
    value t < target = (count t + size (left t) + rightLt, rightEq, rightGt)
    where
    (leftLt, leftEq, leftGt) = compareAndCount (left t) target
    (rightLt, rightEq, rightGt) = compareAndCount (right t) target
compareAndCount t@(TreeNode3 {}) target = compareAndCount binaryNode target
    where
    binaryNode = TreeNode2 {
            value = valueLeft t,
            count = countLeft t,
            left = left t,
            right = TreeNode2 {
                value = valueRight t,
                count = countRight t,
                left = center t,
                right = right t,
                size = countRight t + size (center t) + size (right t),
            },
            size t,
        }



getFirst :: (t, t, t) -> t
getFirst (x, _, _) = x

getSecond :: (t, t, t) -> t
getSecond (_, x, _) = x

getThird :: (t, t, t) -> t
getThird (_, _, x) = x


countLt :: (Ord t) => (AvlTree t) -> t -> Int
countLt = getFirst . compareAndCount

countLte :: (Ord t) => (AvlTree t) -> t -> Int
countLte t target = size t - countGt t

countEq :: (Ord t) => (AvlTree t) -> t -> Int
countEq = getSecond . compareAndCount

countGte :: (Ord t) => (AvlTree t) -> t -> Int
countGte t target =size t - countLt t

countGt :: (Ord t) => (AvlTree t) -> t -> Int
countGt = getThird . compareAndCount




main = print "asd"